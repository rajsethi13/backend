# Development Setup

Easy to use VSCode devcontainer setup is available that configures backing services and tools required for development

Clone the repo, copy example configs and re-open in vscode devcontainer

```shell
git clone https://gitlab.com/castlecraft/rauth/backend
cp -R backend/devcontainer-example backend/.devcontainer
cp -R backend/vscode-example backend/.vscode
code backend
```

Once opened in devcontainer:

```shell
yarn
cp .env.devcontainer .env
yarn start:debug
```

Run the setup setup script. Make sure you have started frontend on port `4120` before running setup.

```shell
yarn ts-node scripts/setup.ts
```

Note:

- Default `ISSUER_URL` is `http://accounts.localhost:4210`.
- Admin user is `admin@example.com` and password is `14CharP@ssword`.
- In case you need to develop around kerberos use `https://accounts.example.com:4210` for `ISSUER_URL` and use `yarn start --ssl` to start frontend.

## Non container setup

In this case it is assumed you are setting up the backing services separately, configure the `.env` file as per your hosts and start the development.

# Lint, format and tests

Merge requests are tested for format, lint, unit and e2e tests.

To format code

```shell
yarn format
```

To lint code

```shell
yarn lint --fix
```

To run unit tests

```shell
yarn test:server
```

To run e2e tests, first clean up the database.

```shell
yarn ts-node scripts/cleanup.ts
```

Note:

- The above command is configured to run with devcontainer setup and cleans e2e test db.
- Set and use `AUTH_SOURCE=admin TEST_MONGO_URI=mongodb://root:admin@mongo:27017/authorization-server` to clean up development database.

Run tests

```shell
yarn test:e2e
```

# Maintenance Guide

## Releases

Maintainers can run the release script to bump version, commit publish message and tag and push commit and tags to remote.

```shell
yarn ts-node scripts/release.ts [major|minor|patch]
```

Note: If release type is anything other than major, minor or patch the script will throw error.

## Dependency Upgrades

Run following command to upgrade dependencies to latest version.

```shell
yarn upgrade-interactive --latest
```

Once the dependencies are upgraded run the commands from Lint, format and tests section to verify successful upgrade.
