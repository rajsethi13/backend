FROM node:20-bullseye AS builder

# Copy and build app
COPY . /home/rauth/backend
WORKDIR /home/rauth/backend
RUN yarn \
    && yarn build \
    && yarn --production=true

FROM node:20-bullseye-slim

# Install dependencies
RUN apt-get update && apt-get install -y \
        gettext-base \
        krb5-user \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Setup entrypoint
COPY container/entrypoint.sh /usr/local/bin/entrypoint.sh

# Add non root user and make entrypoint executable.
RUN useradd -ms /bin/bash rauth && chmod +x /usr/local/bin/entrypoint.sh
WORKDIR /home/rauth/backend

COPY --from=builder /home/rauth/backend .
RUN chown -R rauth:rauth /home/rauth

USER rauth

ENTRYPOINT ["entrypoint.sh"]
CMD ["start"]
