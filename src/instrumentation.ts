import { NodeSDK } from '@opentelemetry/sdk-node';
import {
  ConsoleSpanExporter,
  SimpleSpanProcessor,
} from '@opentelemetry/sdk-trace-node';
import { getNodeAutoInstrumentations } from '@opentelemetry/auto-instrumentations-node';

const sdk = new NodeSDK({
  traceExporter: new ConsoleSpanExporter(),
  spanProcessor: new SimpleSpanProcessor(new ConsoleSpanExporter()),
  instrumentations: [
    getNodeAutoInstrumentations({
      '@opentelemetry/instrumentation-fs': {
        enabled: false,
      },
      '@opentelemetry/instrumentation-mongodb': {
        enhancedDatabaseReporting: true,
      },
      '@opentelemetry/instrumentation-express': {
        requestHook: (spanInfo, requestInfo) => {
          spanInfo.setAttribute(
            'express.body',
            JSON.stringify(requestInfo.request.body),
          );
        },
      },
      '@opentelemetry/instrumentation-http': {
        ignoreIncomingPaths: [/\/metrics/, /\/intake/],
      },
    }),
  ],
});

sdk.start();
