import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const schema = new mongoose.Schema(
  {
    name: String,
    description: String,
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    url: String,
    adminDn: String,
    adminPassword: String,
    userSearchBase: String,
    usernameAttribute: String,
    emailAttribute: String,
    phoneAttribute: String,
    fullNameAttribute: String,
    attributes: [String],
    scope: String,
    clientId: String,
    createdBy: String,
    modifiedBy: String,
    creation: { type: Date, default: () => new Date() },
    modified: Date,
    disabled: { type: Boolean, default: false },
  },
  { collection: 'ldap_client', versionKey: false },
);

export const LDAPClient = schema;

export const LDAP_CLIENT = 'LDAPClient';

export const LDAPServerModel = mongoose.model(LDAP_CLIENT, LDAPClient);
