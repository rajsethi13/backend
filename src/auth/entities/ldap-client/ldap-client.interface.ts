import { Document } from 'mongoose';

export interface LDAPClient extends Document {
  name?: string;
  description?: string;
  uuid?: string;
  url?: string;
  adminDn?: string;
  adminPassword?: string;
  userSearchBase?: string;
  usernameAttribute?: string;
  emailAttribute?: string;
  phoneAttribute?: string;
  fullNameAttribute?: string;
  attributes?: string[];
  scope?: string;
  clientId: string;
  createdBy?: string;
  modifiedBy?: string;
  creation?: Date;
  modified?: Date;
  disabled?: boolean;
}
