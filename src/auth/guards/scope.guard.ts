import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SCOPE } from '../decorators/scope.decorator';

@Injectable()
export class ScopeGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const allowedScope = this.reflector.get<string[]>(
      SCOPE,
      context.getHandler(),
    );
    if (!allowedScope) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    if (request?.token) {
      const tokenScope = request.token.scope || [];
      const hasRole = () =>
        tokenScope.some(scope => allowedScope.includes(scope));
      return tokenScope && hasRole();
    }
  }
}
