import { Test, TestingModule } from '@nestjs/testing';
import { KerberosAuthAggregateService } from './kerberos-auth-aggregate.service';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClientAggregateService } from '../ldap-aggregate/ldap-client-aggregate.service';

describe('KerberosAuthAggregateService', () => {
  let service: KerberosAuthAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        KerberosAuthAggregateService,
        { provide: KerberosRealmService, useValue: {} },
        { provide: LDAPClientAggregateService, useValue: {} },
      ],
    }).compile();

    service = module.get<KerberosAuthAggregateService>(
      KerberosAuthAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
