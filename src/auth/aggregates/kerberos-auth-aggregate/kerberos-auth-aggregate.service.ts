import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { KerberosServer, initializeServer } from 'kerberos';
import { Request } from 'express';
import { v4 as uuidv4 } from 'uuid';

import { KerberosStrategyOptions } from '../../passport/base/kerberos.strategy';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';
import { KerberosRealmRemovedEvent } from '../../events/kerberos-realm-removed/kerberos-realm-removed.event';
import { KerberosRealmAddedEvent } from '../../events/kerberos-realm-added/kerberos-realm-added.event';
import { KerberosRealmModifiedEvent } from '../../events/kerberos-realm-modified/kerberos-realm-modified.event';
import {
  LDAPClientAggregateService,
  LDAPUser,
} from '../ldap-aggregate/ldap-client-aggregate.service';
import { NEGOTIATE } from '../../../constants/app-strings';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { User } from '../../../user-management/entities/user/user.interface';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

@Injectable()
export class KerberosAuthAggregateService extends AggregateRoot {
  constructor(
    private readonly kerberosRealm: KerberosRealmService,
    private readonly ldapClient: LDAPClientAggregateService,
  ) {
    super();
  }

  async loginViaKerberos(
    req: Request,
    auth: string,
    options?: KerberosStrategyOptions,
  ) {
    const realm = await this.kerberosRealm.findOne({
      uuid: req?.params?.kerberosRealm,
    });
    if (!realm) {
      throw new NotFoundException({
        KerberosRealm: req?.params?.kerberosRealm,
      });
    }
    if (realm.servicePrincipalName) {
      if (!options) {
        options = {};
      }
      options.servicePrincipalName = realm.servicePrincipalName;
    }
    const context = await this.fetchKerberosServerContext(options);
    const principal = await this.fetchPrincipal(context, auth);
    const client = await this.ldapClient.findOne({ uuid: realm.ldapClient });
    const ldapUser = await this.getLDAPUser(client, principal);
    const user = await this.ldapClient.getLocalUser(ldapUser, client);
    if (!user) {
      return this.createUser(ldapUser, client);
    }
    return user;
  }

  fetchKerberosServerContext(options?: KerberosStrategyOptions) {
    return new Promise<KerberosServer>((resolve, reject) => {
      initializeServer(
        options?.servicePrincipalName || 'HTTP',
        (err, context) => {
          if (err) {
            return reject(new Error(err));
          }
          return resolve(context);
        },
      );
    });
  }

  fetchPrincipal(context: KerberosServer, auth: string) {
    return new Promise<string>((resolve, reject) => {
      context.step(auth.substring(`${NEGOTIATE} `.length), err => {
        if (err) {
          return reject(new Error(err));
        }
        return resolve(context.username);
      });
    });
  }

  async removeKerberosRealm(uuid: string, userUuid: string) {
    const realm = await this.kerberosRealm.findOne({ uuid });
    if (realm) {
      this.apply(new KerberosRealmRemovedEvent(userUuid, realm));
    } else {
      throw new NotFoundException({ uuid });
    }
  }

  async addKerberosRealm(payload: CreateKerberosRealmDto, createdBy: string) {
    const params = {
      ...payload,
      ...{
        createdBy,
        creation: new Date(),
      },
    } as KerberosRealm;
    this.apply(new KerberosRealmAddedEvent(params));
    return params;
  }

  async modifyKerberosRealm(payload: CreateKerberosRealmDto, uuid: string) {
    const realm = await this.kerberosRealm.findOne({ uuid });
    if (!realm) {
      throw new NotFoundException({ uuid });
    }
    if (payload.ldapClient) {
      const ldapClient = await this.ldapClient.findOne({
        uuid: payload.ldapClient,
      });
      if (!ldapClient) {
        throw new NotFoundException({ LDAPClient: payload.ldapClient });
      }
    }
    Object.assign(realm, payload);
    realm.modified = new Date();
    this.apply(new KerberosRealmModifiedEvent(realm));
    return realm;
  }

  async getLDAPUser(client: LDAPClient, principal: string): Promise<LDAPUser> {
    const username = principal.replace(/@.*$/, '');
    const ldapUser = await this.ldapClient.authenticate({
      username,
      ldapOpts: { url: client.url },
      adminDn: client.adminDn,
      adminPassword: client.adminPassword,
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
      verifyUserExists: true,
    });
    return ldapUser as LDAPUser;
  }

  createUser(ldapUser: LDAPUser, client: LDAPClient) {
    const user = {
      email: ldapUser[client.emailAttribute],
      phone: ldapUser[client.phoneAttribute],
      name: ldapUser[client.fullNameAttribute],
      isEmailVerified: true,
      uuid: uuidv4(),
    } as User;
    user.email = user.email?.trim().toLowerCase();

    this.apply(new UserAccountAddedEvent(user));
    return user;
  }
}
