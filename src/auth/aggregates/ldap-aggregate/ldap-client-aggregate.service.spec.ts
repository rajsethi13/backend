import { Test, TestingModule } from '@nestjs/testing';
import { OnlyAllowValidScopeService } from '../../../client-management/policies';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { AuthService } from '../../controllers/auth/auth.service';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClientAggregateService } from './ldap-client-aggregate.service';

describe('LDAPAggregateService', () => {
  let service: LDAPClientAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LDAPClientAggregateService,
        { provide: UserManagementService, useValue: {} },
        { provide: LDAPClientService, useValue: {} },
        { provide: KerberosRealmService, useValue: {} },
        { provide: OnlyAllowValidScopeService, useValue: {} },
        { provide: AuthService, useValue: {} },
      ],
    }).compile();

    service = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
