import {
  BadGatewayException,
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Client, ClientOptions, EqualityFilter, createClient } from 'ldapjs';
import { v4 as uuidv4 } from 'uuid';

import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { OnlyAllowValidScopeService } from '../../../client-management/policies';
import { InvalidUserException } from '../../../common/filters/exceptions';
import { SCOPE_OPENID } from '../../../constants/app-strings';
import { SystemLogCode } from '../../../system-settings/entities/system-log/system-log.interface';
import { UserManagementService } from '../../../user-management/aggregates/user-management/user-management.service';
import { User } from '../../../user-management/entities/user/user.interface';
import { UserAccountAddedEvent } from '../../../user-management/events/user-account-added/user-account-added.event';
import { AuthService } from '../../controllers/auth/auth.service';
import { CreateLDAPClientDto } from '../../controllers/ldap-client/ldap-client-create.dto';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { LDAPClientAddedEvent } from '../../events/ldap-client-added/ldap-client-added.event';
import { LDAPClientModifiedEvent } from '../../events/ldap-client-modified/ldap-client-modified.event';
import { LDAPClientRemovedEvent } from '../../events/ldap-client-removed/ldap-client-removed.event';
import { LDAPUserClaimsAddedEvent } from '../../events/ldap-user-claims-added/ldap-user-claims-added.event';
import { LDAPUserVerifiedEvent } from '../../events/ldap-user-verified/ldap-user-verified.event';

export interface LDAPUser {
  dn: string;
  [key: string]: string | string[] | undefined;
}

export interface SearchResultEntry {
  type: string;
  objectName: string;
  attributes: {
    type: string;
    values: string[];
  }[];
}

export interface AuthenticationOptions {
  ldapOpts: ClientOptions;
  userDn?: string;
  adminDn?: string;
  adminPassword?: string;
  userSearchBase?: string;
  usernameAttribute?: string;
  username?: string;
  verifyUserExists?: boolean;
  starttls?: boolean;
  groupsSearchBase?: string;
  groupClass?: string;
  groupMemberAttribute?: string;
  groupMemberUserAttribute?: string;
  userPassword?: string;
  attributes?: string[];
}

@Injectable()
export class LDAPClientAggregateService extends AggregateRoot {
  constructor(
    private readonly userManager: UserManagementService,
    private readonly ldap: LDAPClientService,
    private readonly kerberosRealm: KerberosRealmService,
    private readonly scopePolicy: OnlyAllowValidScopeService,
    private readonly authService: AuthService,
  ) {
    super();
  }

  searchResultToUser(pojo: SearchResultEntry) {
    const user = { dn: pojo.objectName };
    pojo.attributes.forEach(attribute => {
      user[attribute.type] =
        attribute.values.length === 1 ? attribute.values[0] : attribute.values;
    });
    return user;
  }

  ldapBind(
    dn: string,
    password: string,
    starttls: boolean,
    ldapOpts: ClientOptions,
  ): Promise<Client> {
    return new Promise(function (resolve, reject) {
      ldapOpts.connectTimeout = ldapOpts.connectTimeout || 5000;
      const client = createClient(ldapOpts);

      client.on('connect', function () {
        if (starttls) {
          client.starttls(ldapOpts.tlsOptions, null, function (error) {
            if (error) {
              reject(new BadGatewayException(error.message));
              return;
            }
            client.bind(dn, password, function (err) {
              if (err) {
                reject(new BadGatewayException(err.message));
                client.unbind();
                return;
              }
              ldapOpts.log && ldapOpts.log.trace('bind success!');
              resolve(client);
            });
          });
        } else {
          client.bind(dn, password, function (err) {
            if (err) {
              reject(new UnauthorizedException(err.message));
              client.unbind();
              return;
            }
            ldapOpts.log && ldapOpts.log.trace('bind success!');
            resolve(client);
          });
        }
      });

      client.on('timeout', err => {
        reject(new BadGatewayException(err.message));
      });
      client.on('connectTimeout', err => {
        reject(new BadGatewayException(err.message));
      });
      client.on('error', err => {
        reject(new BadGatewayException(err.message));
      });

      client.on('connectError', function (error) {
        if (error) {
          reject(new BadGatewayException(error.message));
          return;
        }
      });
    });
  }

  searchUser(
    ldapClient,
    searchBase,
    usernameAttribute,
    username,
    attributes = null,
  ): Promise<LDAPUser> {
    const self = this;
    return new Promise(function (resolve, reject) {
      const filter = new EqualityFilter({
        attribute: usernameAttribute,
        value: username,
      });
      const searchOptions = {
        filter,
        scope: 'sub',
        attributes,
      };
      if (attributes) {
        searchOptions.attributes = attributes;
      }
      ldapClient.search(searchBase, searchOptions, function (err, res) {
        let user = null;
        if (err) {
          reject(new UnauthorizedException(err.message));
          ldapClient.unbind();
          return;
        }
        res.on('searchEntry', function (entry) {
          user = self.searchResultToUser(entry.pojo);
        });
        res.on('searchReference', function (referral) {
          // TODO: we don't support reference yet
          // If the server was able to locate the entry referred to by the baseObject
          // but could not search one or more non-local entries,
          // the server may return one or more SearchResultReference messages,
          // each containing a reference to another set of servers for continuing the operation.
          // referral.uris
        });
        res.on('error', function (err) {
          reject(new UnauthorizedException(err.message));
          ldapClient.unbind();
        });
        res.on('end', function (result) {
          if (result.status !== 0) {
            reject(
              new UnauthorizedException(
                'ldap search status is not 0, search failed',
              ),
            );
          } else {
            resolve(user);
          }
          ldapClient.unbind();
        });
      });
    });
  }

  searchUserGroups(
    ldapClient,
    searchBase,
    user,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
  ): Promise<string[]> {
    return new Promise(function (resolve, reject) {
      ldapClient.search(
        searchBase,
        {
          filter: `(&(objectclass=${groupClass})(${groupMemberAttribute}=${user[groupMemberUserAttribute]}))`,
          scope: 'sub',
        },
        function (err, res) {
          const groups = [];
          if (err) {
            reject(new UnauthorizedException(err.message));
            ldapClient.unbind();
            return;
          }
          res.on('searchEntry', function (entry) {
            groups.push(entry.pojo);
          });
          res.on('searchReference', function (referral) {});
          res.on('error', function (err) {
            reject(new UnauthorizedException(err.message));
            ldapClient.unbind();
          });
          res.on('end', function (result) {
            if (result.status !== 0) {
              reject(
                new UnauthorizedException(
                  'ldap search status is not 0, search failed',
                ),
              );
            } else {
              resolve(groups);
            }
            ldapClient.unbind();
          });
        },
      );
    });
  }

  async authenticateWithAdmin(
    adminDn,
    adminPassword,
    userSearchBase,
    usernameAttribute,
    username,
    userPassword,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    const ldapAdminClient = await this.ldapBind(
      adminDn,
      adminPassword,
      starttls,
      ldapOpts,
    );
    const user = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    ldapAdminClient.unbind();
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `admin did not find user! (${usernameAttribute}=${username})`,
        );
      throw new UnauthorizedException(
        'user not found or username attribute is wrong',
      );
    }
    const userDn = user.dn;
    const ldapUserClient = await this.ldapBind(
      userDn,
      userPassword,
      starttls,
      ldapOpts,
    );
    ldapUserClient.unbind();
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      const ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        starttls,
        ldapOpts,
      );
      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapAdminClient.unbind();
    }
    return user;
  }

  async authenticateWithUser(
    userDn,
    userSearchBase,
    usernameAttribute,
    username,
    userPassword,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    const ldapUserClient = await this.ldapBind(
      userDn,
      userPassword,
      starttls,
      ldapOpts,
    );
    if (!usernameAttribute || !userSearchBase) {
      // if usernameAttribute is not provided, no user detail is needed.
      ldapUserClient.unbind();
      return true;
    }
    const user = await this.searchUser(
      ldapUserClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `user logged in, but user details could not be found. (${usernameAttribute}=${username}). Probabaly wrong attribute or searchBase?`,
        );
      throw new UnauthorizedException(
        'user logged in, but user details could not be found. Probabaly usernameAttribute or userSearchBase is wrong?',
      );
    }
    ldapUserClient.unbind();
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      const ldapUserClient = await this.ldapBind(
        userDn,
        userPassword,
        starttls,
        ldapOpts,
      );
      const groups = await this.searchUserGroups(
        ldapUserClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapUserClient.unbind();
    }
    return user;
  }

  async verifyUserExists(
    adminDn,
    adminPassword,
    userSearchBase,
    usernameAttribute,
    username,
    starttls,
    ldapOpts,
    groupsSearchBase,
    groupClass,
    groupMemberAttribute = 'member',
    groupMemberUserAttribute = 'dn',
    attributes = null,
  ) {
    const ldapAdminClient = await this.ldapBind(
      adminDn,
      adminPassword,
      starttls,
      ldapOpts,
    );
    const user = await this.searchUser(
      ldapAdminClient,
      userSearchBase,
      usernameAttribute,
      username,
      attributes,
    );
    ldapAdminClient.unbind();
    if (!user || !user.dn) {
      ldapOpts.log &&
        ldapOpts.log.trace(
          `admin did not find user! (${usernameAttribute}=${username})`,
        );
      throw new UnauthorizedException(
        'user not found or username attribute is wrong',
      );
    }
    if (groupsSearchBase && groupClass && groupMemberAttribute) {
      const ldapAdminClient = await this.ldapBind(
        adminDn,
        adminPassword,
        starttls,
        ldapOpts,
      );
      const groups = await this.searchUserGroups(
        ldapAdminClient,
        groupsSearchBase,
        user,
        groupClass,
        groupMemberAttribute,
        groupMemberUserAttribute,
      );
      user.groups = groups;
      ldapAdminClient.unbind();
    }
    return user;
  }

  async authenticate(options: AuthenticationOptions) {
    if (!options.userDn) {
      if (!options.adminDn) {
        throw new UnauthorizedException('Admin mode adminDn must be provided');
      }
      if (!options.adminPassword) {
        throw new UnauthorizedException(
          'Admin mode adminPassword must be provided',
        );
      }
      if (!options.userSearchBase) {
        throw new UnauthorizedException(
          'Admin mode userSearchBase must be provided',
        );
      }
      if (!options.usernameAttribute) {
        throw new UnauthorizedException(
          'Admin mode usernameAttribute must be provided',
        );
      }
      if (!options.username) {
        throw new UnauthorizedException('Admin mode username must be provided');
      }
    }
    if (!options.ldapOpts && options.ldapOpts.url) {
      throw new UnauthorizedException('ldapOpts.url must be provided');
    }
    if (options.verifyUserExists) {
      if (!options.adminDn) {
        throw new UnauthorizedException('Admin mode adminDn must be provided');
      }
      if (!options.adminPassword) {
        throw new UnauthorizedException(
          'adminDn and adminPassword must be both provided.',
        );
      }
      return await this.verifyUserExists(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.starttls,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
      );
    }
    if (!options.userPassword) {
      throw new UnauthorizedException('userPassword must be provided');
    }
    if (options.adminDn) {
      if (!options.adminPassword) {
        throw new UnauthorizedException(
          'adminDn and adminPassword must be both provided.',
        );
      }
      return await this.authenticateWithAdmin(
        options.adminDn,
        options.adminPassword,
        options.userSearchBase,
        options.usernameAttribute,
        options.username,
        options.userPassword,
        options.starttls,
        options.ldapOpts,
        options.groupsSearchBase,
        options.groupClass,
        options.groupMemberAttribute,
        options.groupMemberUserAttribute,
        options.attributes,
      );
    }
    if (!options.userDn) {
      throw new UnauthorizedException(
        'adminDn/adminPassword OR userDn must be provided',
      );
    }
    return await this.authenticateWithUser(
      options.userDn,
      options.userSearchBase,
      options.usernameAttribute,
      options.username,
      options.userPassword,
      options.starttls,
      options.ldapOpts,
      options.groupsSearchBase,
      options.groupClass,
      options.groupMemberAttribute,
      options.groupMemberUserAttribute,
      options.attributes,
    );
  }

  async getValidClient(uuid: string) {
    const client = await this.ldap.findOne({ uuid });
    if (!client) {
      throw new BadRequestException({ InvalidLDAPClient: uuid });
    }
    return client;
  }

  async loginViaLDAP(
    ldapClient: string,
    username: string,
    userPassword: string,
    code?: string,
  ) {
    const client = await this.getValidClient(ldapClient);

    const user = await this.getLocalUser(
      (await this.verifyUser(client, username)) as LDAPUser,
      client,
    );

    if (!user) {
      throw new InvalidUserException();
    }

    await this.authService.validateLoginAttempts(user);

    const attributes = [
      client.emailAttribute,
      client.phoneAttribute,
      client.fullNameAttribute,
    ];
    if (client.attributes?.length) {
      for (const attr of client.attributes) {
        attributes.push(attr);
      }
    }
    try {
      const ldapUser = (await this.authenticate({
        username,
        userPassword,
        ldapOpts: { url: client.url },
        adminDn: client.adminDn,
        adminPassword: client.adminPassword,
        userSearchBase: client.userSearchBase,
        usernameAttribute: client.usernameAttribute,
        attributes,
      })) as LDAPUser;
      const localUser = await this.getLocalUser(ldapUser, client);
      if (localUser) {
        await this.loginWith2Fa(localUser, code);
        await this.authService.clearAuthFailLogs(localUser.uuid);
        return localUser;
      }
      return this.createUser(ldapUser, client);
    } catch (error) {
      const err = new UnauthorizedException(error.message);
      await this.authService.saveAuthLog(
        user.uuid,
        SystemLogCode.Failure,
        error.message,
      );
      throw err;
    }
  }

  async verifyUser(
    client: LDAPClient,
    username: string,
    userPassword?: string,
  ) {
    return await this.authenticate({
      username,
      ldapOpts: { url: client.url },
      adminDn: client.adminDn,
      adminPassword: client.adminPassword,
      userSearchBase: client.userSearchBase,
      usernameAttribute: client.usernameAttribute,
      userPassword,
      verifyUserExists: userPassword ? false : true,
    });
  }

  async verifyViaLDAP(ldapClient: string, username: string, password?: string) {
    const client = await this.getValidClient(ldapClient);
    const ldapUser = (await this.verifyUser(client, username)) as LDAPUser;

    if (ldapUser) {
      const verifiedOut: { user?: User } = {};
      const localUser = await this.getLocalUser(ldapUser, client);
      if (localUser) {
        try {
          if (password) {
            await this.authService.validateLoginAttempts(localUser);
            await this.verifyUser(client, username, password);
          }
          verifiedOut.user = localUser;
        } catch (error) {
          const err = new UnauthorizedException(error.message);
          await this.authService.saveAuthLog(
            localUser.uuid,
            SystemLogCode.Failure,
            error.message,
          );
          throw err;
        }
      }
      if (!localUser) {
        verifiedOut.user = this.createUser(ldapUser, client);
      }
      this.apply(new LDAPUserVerifiedEvent(ldapUser, client));
      return verifiedOut;
    }
  }

  createUser(ldapUser: LDAPUser, client: LDAPClient) {
    const user = {
      email: ldapUser[client.emailAttribute],
      phone: ldapUser[client.phoneAttribute],
      name: ldapUser[client.fullNameAttribute],
      isEmailVerified: true,
      uuid: uuidv4(),
    } as User;
    user.email = user.email?.trim().toLowerCase();
    this.apply(new UserAccountAddedEvent(user));
    this.addUserClaims(user, client, ldapUser);
    return user;
  }

  async getLocalUser(ldapUser: LDAPUser, client: LDAPClient) {
    const email = ldapUser[client.emailAttribute];
    const phone = ldapUser[client.phoneAttribute];
    if (!phone && !email) {
      throw new BadRequestException({ MissingPhoneOrEmail: { phone, email } });
    }
    const userEmail = await this.userManager.findByEmail(
      (email as string).trim().toLowerCase(),
    );
    const userPhone = await this.userManager.findByPhone(phone as string);

    if (userEmail && userPhone) {
      // Found phone and email locally

      if (userEmail.uuid === userPhone.uuid) {
        // phone and email belong to same user

        return userEmail;
      }

      if (userEmail.uuid !== userPhone.uuid) {
        // phone and email do not belong to same user
        // set priority to email (TODO: check from server)

        return userEmail;
      }
    } else if (userEmail) {
      // Select email if only email found

      return userEmail;
    } else if (userPhone) {
      // Select phone if only phone found

      return userPhone;
    }
  }

  async removeLDAPClient(uuid: string, userUuid: string) {
    const ldapClient = await this.ldap.findOne({ uuid });
    if (ldapClient) {
      const kerberosRealm = await this.kerberosRealm.findOne({
        ldapClient: ldapClient.uuid,
      });
      if (kerberosRealm) {
        throw new BadRequestException({
          KerberosRealm: {
            uuid: kerberosRealm.uuid,
            ldapClient: kerberosRealm.ldapClient,
          },
        });
      }
      this.apply(new LDAPClientRemovedEvent(userUuid, ldapClient));
    } else {
      throw new NotFoundException({ LDAPClient: uuid });
    }
  }

  async addLDAPClient(payload: CreateLDAPClientDto, createdBy: string) {
    if (payload.scope) {
      await this.scopePolicy.validate([payload.scope]);
    }
    const params = {
      ...payload,
      ...{
        createdBy,
        creation: new Date(),
      },
    } as LDAPClient;
    this.apply(new LDAPClientAddedEvent(params));
    return params;
  }

  async modifyLDAPClient(payload: CreateLDAPClientDto, uuid: string) {
    const ldapClient = await this.ldap.findOne({ uuid });
    if (!ldapClient) {
      throw new NotFoundException({ LDAPClient: uuid });
    }
    if (payload.scope) {
      await this.scopePolicy.validate([payload.scope]);
    }
    Object.assign(ldapClient, payload);
    ldapClient.modified = new Date();
    this.apply(new LDAPClientModifiedEvent(ldapClient));
    return ldapClient;
  }

  async findOne(params) {
    return await this.ldap.findOne(params);
  }

  addUserClaims(user: User, client: LDAPClient, ldapUser: LDAPUser) {
    const claims = [
      {
        scope: client.scope || SCOPE_OPENID,
        name: client.usernameAttribute,
        uuid: user.uuid,
        value: ldapUser[client.usernameAttribute],
      },
    ] as UserClaim[];
    for (const attribute of client.attributes) {
      if (ldapUser[attribute]) {
        claims.push({
          scope: client.scope || SCOPE_OPENID,
          name: attribute,
          uuid: user.uuid,
          value: ldapUser[attribute],
        } as UserClaim);
      }
    }
    if (claims.length > 0) {
      this.apply(new LDAPUserClaimsAddedEvent(claims));
    }
  }

  async loginWith2Fa(user: User, code?: string) {
    if (user.enable2fa) {
      await this.authService.validate2FaCode(user, code);
    }
  }
}
