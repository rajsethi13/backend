import { Injectable, NestMiddleware } from '@nestjs/common';
import { stringify } from 'querystring';
import { ServerSettingsService } from '../../system-settings/entities/server-settings/server-settings.service';
import { UserService } from '../../user-management/entities/user/user.service';
import { addSessionUser } from '../guards/guard.utils';
import {
  ACCOUNT_CHOOSE_ROUTE,
  LOGIN,
  LOGIN_ROUTE,
  SELECT_ACCOUNT,
} from '../../constants/app-strings';
import { OAUTH2_CONFIRMATION_ENDPOINT } from '../../constants/url-strings';

@Injectable()
export class ChooseAccountMiddleware implements NestMiddleware {
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly user: UserService,
  ) {}

  async use(req: any, res: any, next: () => void) {
    const settings = await this.settings.find();

    const queryPrompt = req.query.prompt;
    delete req.query.prompt;
    const reqQueryStr = stringify(req.query);
    if (queryPrompt === LOGIN) {
      const location = encodeURIComponent(
        settings.issuerUrl + OAUTH2_CONFIRMATION_ENDPOINT + '?' + reqQueryStr,
      );
      const redirectTo =
        settings.issuerUrl + LOGIN_ROUTE + '?redirect=' + location;
      return res.redirect(redirectTo);
    }

    if (!settings.enableChoosingAccount) {
      return next();
    }

    if (!req.session.users) {
      req.session.users = [];
    }

    const reqUser = req.session.users.find(user => {
      if (user.uuid === req.session.selectedUser) {
        return user;
      }
    });

    if (reqUser) {
      const user = await this.user.findOne({ uuid: reqUser.uuid });
      const users = req.session?.users;
      req.logIn(user, () => {
        req.session.users = users;
        addSessionUser(req, {
          uuid: user.uuid,
          email: user.email,
          phone: user.phone,
        });
      });
      delete req.session.selectedUser;
    }

    if (queryPrompt === SELECT_ACCOUNT) {
      return res.redirect(
        settings.issuerUrl + ACCOUNT_CHOOSE_ROUTE + '?' + reqQueryStr,
      );
    }

    return next();
  }
}
