import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { KerberosRealmModifiedEvent } from './kerberos-realm-modified.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';

@EventsHandler(KerberosRealmModifiedEvent)
export class KerberosRealmModifiedHandler
  implements IEventHandler<KerberosRealmModifiedEvent>
{
  constructor(private readonly realm: KerberosRealmService) {}
  handle(event: KerberosRealmModifiedEvent) {
    this.realm
      .save(event.realm)
      .then(modified => {})
      .catch(error => {});
  }
}
