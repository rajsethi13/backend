import { IEvent } from '@nestjs/cqrs';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';

export class KerberosRealmModifiedEvent implements IEvent {
  constructor(public readonly realm: KerberosRealm) {}
}
