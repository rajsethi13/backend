import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPClientAddedEvent } from './ldap-client-added.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';

@EventsHandler(LDAPClientAddedEvent)
export class LDAPClientAddedHandler
  implements IEventHandler<LDAPClientAddedEvent>
{
  constructor(private readonly ldapClient: LDAPClientService) {}
  handle(event: LDAPClientAddedEvent) {
    this.ldapClient
      .save(event.ldapClient)
      .then(added => {})
      .catch(error => {});
  }
}
