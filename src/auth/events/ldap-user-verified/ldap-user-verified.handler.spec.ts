import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { HttpModule } from '@nestjs/axios';
import { LDAPUserVerifiedHandler } from './ldap-user-verified.handler';
import { LDAPUserVerifiedEvent } from './ldap-user-verified.event';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';
import { UserService } from '../../../user-management/entities/user/user.service';
import { LDAPUser } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

describe('Event: LDAPUserVerifiedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: LDAPUserVerifiedHandler;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule, HttpModule],
      providers: [
        LDAPUserVerifiedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
        { provide: AuthDataService, useValue: {} },
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<LDAPUserVerifiedHandler>(LDAPUserVerifiedHandler);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should publish event', async () => {
    eventBus$.publish = jest.fn(() => {});
    eventHandler.handle = jest.fn(() => {});
    await eventHandler.handle(
      new LDAPUserVerifiedEvent({} as LDAPUser, {} as LDAPClient),
    );
    expect(eventHandler.handle).toHaveBeenCalledTimes(1);
  });
});
