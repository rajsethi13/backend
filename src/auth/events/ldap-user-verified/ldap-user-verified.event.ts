import { IEvent } from '@nestjs/cqrs';
import { LDAPUser } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';

export class LDAPUserVerifiedEvent implements IEvent {
  constructor(
    public readonly LDAPUser: LDAPUser,
    public readonly ldapClient: LDAPClient,
  ) {}
}
