import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { KerberosRealmRemovedEvent } from './kerberos-realm-removed.event';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';

@EventsHandler(KerberosRealmRemovedEvent)
export class KerberosRealmRemovedHandler
  implements IEventHandler<KerberosRealmRemovedEvent>
{
  constructor(private readonly realm: KerberosRealmService) {}
  handle(event: KerberosRealmRemovedEvent) {
    const { realm } = event;
    from(this.realm.remove(realm)).subscribe({
      next: success => {},
      error: error => {},
    });
  }
}
