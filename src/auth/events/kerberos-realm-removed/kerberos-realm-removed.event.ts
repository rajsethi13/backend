import { IEvent } from '@nestjs/cqrs';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';

export class KerberosRealmRemovedEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly realm: KerberosRealm,
  ) {}
}
