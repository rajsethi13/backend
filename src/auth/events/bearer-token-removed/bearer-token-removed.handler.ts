import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { HttpService } from '@nestjs/axios';
import { map, retry, switchMap } from 'rxjs/operators';
import { from, lastValueFrom, of, throwError } from 'rxjs';
import jose from 'node-jose';
import { v4 as uuidv4 } from 'uuid';
import { stringify } from 'querystring';
import { BearerTokenRemovedEvent } from './bearer-token-removed.event';
import { ClientService } from '../../../client-management/entities/client/client.service';
import { BearerTokenService } from '../../entities/bearer-token/bearer-token.service';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { OIDCKeyService } from '../../entities/oidc-key/oidc-key.service';
import { JWKSNotFoundException } from '../../../common/filters/exceptions';

@EventsHandler(BearerTokenRemovedEvent)
export class BearerTokenRemovedHandler
  implements IEventHandler<BearerTokenRemovedEvent>
{
  constructor(
    private readonly http: HttpService,
    private readonly client: ClientService,
    private readonly token: BearerTokenService,
    private readonly settings: ServerSettingsService,
    private readonly oidcKey: OIDCKeyService,
  ) {}
  handle(event: BearerTokenRemovedEvent) {
    const { token } = event;
    this.token
      .remove(token)
      .then(removed => {
        if (token.user) {
          return lastValueFrom(
            this.signLogoutToken(token.client, token.user),
          ).then(logoutToken => this.informClients(logoutToken));
        }
      })
      .then(informed => {})
      .catch(error => {});
  }

  async informClients(logoutToken: string) {
    const clients = await this.client.findAll();
    for (const client of clients) {
      if (client.tokenDeleteEndpoint) {
        this.http
          .post(
            client.tokenDeleteEndpoint,
            stringify({ logout_token: logoutToken }),
            {
              headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
              },
            },
          )
          .pipe(retry(3))
          .subscribe({
            next: success => {},
            error: error => {},
          });
      }
    }
  }

  signLogoutToken(clientId: string, sub: string) {
    return from(this.settings.find()).pipe(
      map(settings => {
        const issuedAt = Date.parse(new Date().toString()) / 1000;
        const claims = {
          iss: settings.issuerUrl,
          aud: clientId,
          iat: Math.trunc(issuedAt),
          jti: uuidv4(),
          events: {
            'http://schemas.openid.net/event/backchannel-logout': {},
          },
          sub,
        };
        return claims;
      }),
      switchMap(claims => {
        return from(this.oidcKey.find()).pipe(
          switchMap(jwks => {
            // Pick first or only key from array
            const foundKey = jwks[0];
            if (!foundKey) {
              return throwError(() => new JWKSNotFoundException());
            }
            return of(foundKey).pipe(
              switchMap(foundKey => {
                return from(
                  jose.JWS.createSign(
                    {
                      alg: 'RS256',
                      format: 'compact',
                      kid: foundKey.keyPair.kid,
                    },
                    foundKey.keyPair,
                  )
                    .update(JSON.stringify(claims))
                    .final() as string,
                );
              }),
            );
          }),
        );
      }),
    );
  }
}
