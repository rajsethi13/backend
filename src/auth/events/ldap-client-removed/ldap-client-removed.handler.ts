import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { from } from 'rxjs';
import { LDAPClientRemovedEvent } from './ldap-client-removed.event';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';

@EventsHandler(LDAPClientRemovedEvent)
export class LDAPClientRemovedHandler
  implements IEventHandler<LDAPClientRemovedEvent>
{
  constructor(private readonly ldapClient: LDAPClientService) {}
  handle(event: LDAPClientRemovedEvent) {
    const { ldapClient } = event;
    from(this.ldapClient.remove(ldapClient)).subscribe({
      next: success => {},
      error: error => {},
    });
  }
}
