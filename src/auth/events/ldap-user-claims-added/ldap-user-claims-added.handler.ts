import { IEventHandler, EventsHandler } from '@nestjs/cqrs';
import { LDAPUserClaimsAddedEvent } from './ldap-user-claims-added.event';
import { UserClaimService } from '../../entities/user-claim/user-claim.service';

@EventsHandler(LDAPUserClaimsAddedEvent)
export class LDAPUserClaimsAddedHandler
  implements IEventHandler<LDAPUserClaimsAddedEvent>
{
  constructor(private readonly userClaim: UserClaimService) {}

  handle(event: LDAPUserClaimsAddedEvent) {
    event.claims?.forEach(claim => {
      this.userClaim
        .findOneAndUpdate(
          { scope: claim.scope, name: claim.name, uuid: claim.uuid },
          claim,
          { upsert: true },
        )
        .then(added => {})
        .catch(err => {});
    });
  }
}
