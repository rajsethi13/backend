import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class RequestLogin {
  @IsString()
  @ApiProperty({
    description: i18n.__('Identifies a user uniquely'),
    required: true,
    example: 'john k',
  })
  username: string;
}
