import {
  Module,
  NestModule,
  MiddlewareConsumer,
  Global,
  Inject,
} from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { APP_FILTER } from '@nestjs/core';
import MongoStore from 'rate-limit-mongo';

import { OAuth2orizeSetup } from './middlewares/oauth2orize.setup';
import { OAuth2ConfirmationMiddleware } from './middlewares/oauth2-confirmation.middleware';
import { OAuth2AuthorizationMiddleware } from './middlewares/oauth2-authorization.middleware';
import { OAuth2TokenMiddleware } from './middlewares/oauth2-token.middleware';
import { OAuth2ErrorHandlerMiddleware } from './middlewares/oauth2-errorhandler.middleware';
import { OAuth2DecisionMiddleware } from './middlewares/oauth2-decision.middleware';
import { PassportAuthenticateMiddleware } from './middlewares/passport-authenticate.middleware';
import { CleanOauth2orizeSessionMiddleware } from './middlewares/clean-oauth2orize-session.middleware';
import { SaveDeviceInfoMiddleware } from './middlewares/save-device-info.middleware';
import { ChooseAccountMiddleware } from './middlewares/choose-account.middleware';
import { AuthEntitiesModule } from './entities/entities.module';
import { OAuth2Module } from './oauth2/oauth2.module';
import { PassportModule } from './passport/passport.module';
import { authControllers, authServices } from './controllers';
import { RoleGuard } from './guards/role.guard';
import { ScopeGuard } from './guards/scope.guard';
import { BasicClientCredentialsGuard } from './guards/basic-client-credentials.guard';
import { EnsureLoginGuard } from './guards/ensure-login.guard';
import { AuthAggregates } from './aggregates';
import { AuthCommandHandlers } from './commands';
import { AuthEventHandlers } from './events';
import { AuthQueryHandlers } from './queries';
import { AuthSchedulers } from './schedulers';
import { AuthorizationErrorFilter } from '../common/filters/authorization-error.filter';
import { TokenErrorFilter } from '../common/filters/token-error.filter';
import { OAuth2ErrorFilter } from '../common/filters/oauth2-error.filter';
import { OpenIDAuthorizationErrorFilter } from '../common/filters/openid-authorization-error.filter';
import { rateLimiterMiddleware } from './middlewares/rate-limit.middleware';
import { CustomLoginMiddleware } from './middlewares/custom-login.middleware';
import { RATE_LIMIT_CONNECTION } from '../common/database.provider';
import { OAuth2ClientGuard } from './guards/oauth2-client.guard';
import { LDAPClientGuard } from './guards/ldap-client.guard';
import { KerberosGuard } from './guards/kerberos.guard';
import { AuthServerVerificationGuard } from './guards/authserver-verification.guard';
import {
  ConfigService,
  ENABLE_RATE_LIMIT,
  EnableRateLimit,
} from '../config/config.service';

export const AUTH_MAX_REQUESTS = 10;
export const AUTH_WINDOW = 60 * 1000;
export const AUTH_LOGIN_ROUTE = '/auth/login';
export const AUTH_VERIFY_PASSWORD_ROUTE = '/auth/verify_password';
export const OAUTH2_AUTHORIZE_ROUTE = '/oauth2/authorize';
export const OAUTH2_CONFIRMATION_ROUTE = '/oauth2/confirmation';
export const OAUTH2_TOKEN_ROUTE = '/oauth2/token';

@Global()
@Module({
  imports: [CqrsModule, AuthEntitiesModule, OAuth2Module, PassportModule],
  providers: [
    ...authServices,

    // Middlewares
    OAuth2orizeSetup,
    OAuth2ConfirmationMiddleware,
    OAuth2AuthorizationMiddleware,
    CleanOauth2orizeSessionMiddleware,
    SaveDeviceInfoMiddleware,
    ChooseAccountMiddleware,
    OAuth2TokenMiddleware,
    OAuth2ErrorHandlerMiddleware,
    CustomLoginMiddleware,

    // Scheduled Services
    ...AuthSchedulers,

    // Guards
    RoleGuard,
    ScopeGuard,
    BasicClientCredentialsGuard,
    EnsureLoginGuard,
    OAuth2ClientGuard,
    LDAPClientGuard,
    KerberosGuard,
    AuthServerVerificationGuard,

    // CQRS
    ...AuthAggregates,
    ...AuthCommandHandlers,
    ...AuthEventHandlers,
    ...AuthQueryHandlers,

    // oauth2orize Error Filters
    { provide: APP_FILTER, useClass: AuthorizationErrorFilter },
    { provide: APP_FILTER, useClass: TokenErrorFilter },
    { provide: APP_FILTER, useClass: OAuth2ErrorFilter },
    { provide: APP_FILTER, useClass: OpenIDAuthorizationErrorFilter },
  ],
  controllers: [...authControllers],
  exports: [
    ...authServices,
    ...AuthAggregates,
    ...AuthSchedulers,
    AuthEntitiesModule,
    RoleGuard,
    BasicClientCredentialsGuard,
    CqrsModule,
  ],
})
export class AuthModule implements NestModule {
  constructor(
    @Inject(RATE_LIMIT_CONNECTION)
    private readonly store: MongoStore,
    private readonly config: ConfigService,
  ) {}

  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        CustomLoginMiddleware,
        ChooseAccountMiddleware,
        OAuth2ConfirmationMiddleware,
        CleanOauth2orizeSessionMiddleware,
        SaveDeviceInfoMiddleware,
      )
      .forRoutes(OAUTH2_CONFIRMATION_ROUTE)
      .apply(OAuth2AuthorizationMiddleware, OAuth2DecisionMiddleware)
      .forRoutes(OAUTH2_AUTHORIZE_ROUTE)
      .apply(
        PassportAuthenticateMiddleware,
        OAuth2TokenMiddleware,
        OAuth2ErrorHandlerMiddleware,
      )
      .forRoutes(OAUTH2_TOKEN_ROUTE);

    if (this.config.get(ENABLE_RATE_LIMIT) === EnableRateLimit.ON) {
      consumer
        .apply(
          rateLimiterMiddleware(AUTH_MAX_REQUESTS, AUTH_WINDOW, this.store),
        )
        .forRoutes(AUTH_LOGIN_ROUTE, AUTH_VERIFY_PASSWORD_ROUTE);
    }
  }
}
