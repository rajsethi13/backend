import { Test, TestingModule } from '@nestjs/testing';
import { CqrsModule } from '@nestjs/cqrs';
import { UserService } from '../../../user-management/entities/user/user.service';
import { DeleteUnverifiedPhonesService } from './delete-unverified-phones.service';

describe('DeleteUnverifiedPhonesService', () => {
  let service: DeleteUnverifiedPhonesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        DeleteUnverifiedPhonesService,
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    service = module.get<DeleteUnverifiedPhonesService>(
      DeleteUnverifiedPhonesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
