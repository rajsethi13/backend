import { Injectable } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import cron from 'node-cron';
import { UserService } from '../../../user-management/entities/user/user.service';
import { RemoveUnverifiedUserCommand } from '../../../user-management/commands/remove-unverified-user/remove-unverified-user.command';

export const TWENTY_FOUR_HOURS_MS = 24 * 60 * 60 * 1000;

@Injectable()
export class DeleteUnverifiedPhonesService {
  constructor(
    private readonly user: UserService,
    private readonly commandBus: CommandBus,
  ) {}

  start() {
    // Every 6 hours
    cron.schedule('0 */6 * * *', async () => {
      // Delete 100 unverified phones created 24 hours ago, user must also be disabled.
      const users = await this.user.list(0, 100, undefined, {
        disabled: true,
        unverifiedPhone: { $exists: true },
        isEmailVerified: false,
        creation: { $lt: new Date(Date.now() - TWENTY_FOUR_HOURS_MS) },
      });

      for (const user of users.docs) {
        await this.commandBus.execute(new RemoveUnverifiedUserCommand(user));
      }
    });
  }
}
