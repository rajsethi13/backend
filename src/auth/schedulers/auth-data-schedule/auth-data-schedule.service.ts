import { Injectable } from '@nestjs/common';
import cron from 'node-cron';
import { AuthDataService } from '../../../user-management/entities/auth-data/auth-data.service';

@Injectable()
export class AuthDataScheduleService {
  constructor(private readonly authData: AuthDataService) {}

  start() {
    // Every hour
    cron.schedule('0 * * * *', async () => {
      await this.authData.deleteMany({ expiry: { $lt: new Date() } });
    });
  }
}
