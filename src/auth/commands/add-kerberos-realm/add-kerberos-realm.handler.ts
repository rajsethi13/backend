import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddKerberosRealmCommand } from './add-kerberos-realm.command';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';

@CommandHandler(AddKerberosRealmCommand)
export class AddKerberosRealmHandler
  implements ICommandHandler<AddKerberosRealmCommand>
{
  constructor(
    private readonly manager: KerberosAuthAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: AddKerberosRealmCommand) {
    const { payload, createdBy } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const kerberosRealm = await aggregate.addKerberosRealm(payload, createdBy);
    aggregate.commit();
    return kerberosRealm;
  }
}
