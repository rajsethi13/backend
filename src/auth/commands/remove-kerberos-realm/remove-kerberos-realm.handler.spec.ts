import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { RemoveKerberosRealmHandler } from './remove-kerberos-realm.handler';
import { RemoveKerberosRealmCommand } from './remove-kerberos-realm.command';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';

describe('Command: RemoveLDAPClientHandler', () => {
  let commandBus$: CommandBus;
  let manager: KerberosAuthAggregateService;
  let commandHandler: RemoveKerberosRealmHandler;
  let publisher: EventPublisher;

  const mockRealm = {
    createdBy: '62843fea-1e30-4b1f-af91-3ff582ff9948',
    creation: new Date(),
    uuid: 'f97cef1e-4b15-4194-b40d-7ea0c0d8fa24',
  } as KerberosRealm;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        RemoveKerberosRealmHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosAuthAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<KerberosAuthAggregateService>(
      KerberosAuthAggregateService,
    );
    commandHandler = module.get<RemoveKerberosRealmHandler>(
      RemoveKerberosRealmHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should remove KerberosRealm using the KerberosAuthAggregateService', async () => {
    manager.removeKerberosRealm = jest.fn(() => Promise.resolve());
    publisher.mergeObjectContext = jest
      .fn()
      .mockImplementation((...args) => ({ commit: () => {} }));
    await commandHandler.execute(
      new RemoveKerberosRealmCommand(mockRealm.createdBy, mockRealm.uuid),
    );
    expect(manager.removeKerberosRealm).toHaveBeenCalledTimes(1);
  });
});
