import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddLDAPClientCommand } from './add-ldap-client.command';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

@CommandHandler(AddLDAPClientCommand)
export class AddLDAPClientHandler
  implements ICommandHandler<AddLDAPClientCommand>
{
  constructor(
    private readonly manager: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: AddLDAPClientCommand) {
    const { payload, createdBy } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const ldapClient = await aggregate.addLDAPClient(payload, createdBy);
    aggregate.commit();
    return ldapClient;
  }
}
