import { Test } from '@nestjs/testing';
import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';
import { ModifyKerberosRealmHandler } from './modify-kerberos-realm.handler';
import { ModifyKerberosRealmCommand } from './modify-kerberos-realm.command';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { CreateKerberosRealmDto } from '../../controllers/kerberos/kerberos-realm-create.dto';

describe('Command: ModifyKerberosRealmHandler', () => {
  let commandBus$: CommandBus;
  let manager: KerberosAuthAggregateService;
  let commandHandler: ModifyKerberosRealmHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        ModifyKerberosRealmHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: KerberosAuthAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<KerberosAuthAggregateService>(
      KerberosAuthAggregateService,
    );
    commandHandler = module.get<ModifyKerberosRealmHandler>(
      ModifyKerberosRealmHandler,
    );
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should modify realm using the KerberosAuthAggregateService', async () => {
    const modifyKerberosRealm = jest.fn(() =>
      Promise.resolve({} as LDAPClient & { _id: any }),
    );
    manager.modifyKerberosRealm = modifyKerberosRealm;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      modifyKerberosRealm,
    }));
    await commandHandler.execute(
      new ModifyKerberosRealmCommand({} as CreateKerberosRealmDto, '420'),
    );
    expect(manager.modifyKerberosRealm).toHaveBeenCalledTimes(1);
  });
});
