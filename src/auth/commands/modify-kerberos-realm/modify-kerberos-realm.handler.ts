import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ModifyKerberosRealmCommand } from './modify-kerberos-realm.command';
import { KerberosAuthAggregateService } from '../../aggregates/kerberos-auth-aggregate/kerberos-auth-aggregate.service';

@CommandHandler(ModifyKerberosRealmCommand)
export class ModifyKerberosRealmHandler
  implements ICommandHandler<ModifyKerberosRealmCommand>
{
  constructor(
    private readonly manager: KerberosAuthAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: ModifyKerberosRealmCommand) {
    const { payload, uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const realm = await aggregate.modifyKerberosRealm(payload, uuid);
    aggregate.commit();
    return realm;
  }
}
