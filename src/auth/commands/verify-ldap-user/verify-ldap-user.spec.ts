import { CommandBus, CqrsModule, EventPublisher } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import { randomBytes32 } from '../../../client-management/entities/client/client.schema';
import { User } from '../../../user-management/entities/user/user.interface';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';
import { VerifyLDAPUserCommand } from './verify-ldap-user.command';
import { VerifyLDAPUserHandler } from './verify-ldap-user.handler';

describe('Command: VerifyLDAPUserHandler', () => {
  let commandBus$: CommandBus;
  let manager: LDAPClientAggregateService;
  let commandHandler: VerifyLDAPUserHandler;
  let publisher: EventPublisher;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        VerifyLDAPUserHandler,
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: LDAPClientAggregateService,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    commandBus$ = module.get<CommandBus>(CommandBus);
    manager = module.get<LDAPClientAggregateService>(
      LDAPClientAggregateService,
    );
    commandHandler = module.get<VerifyLDAPUserHandler>(VerifyLDAPUserHandler);
    publisher = module.get<EventPublisher>(EventPublisher);
  });

  it('should be defined', () => {
    expect(commandBus$).toBeDefined();
    expect(manager).toBeDefined();
    expect(commandHandler).toBeDefined();
  });

  it('should verify phone/otp using the LDAPClientAggregateService', async () => {
    const verifyViaLDAP = jest.fn((...args) =>
      Promise.resolve({
        user: {} as User,
        sid: randomBytes32(),
      }),
    );
    manager.verifyViaLDAP = verifyViaLDAP;
    commandBus$.execute = jest.fn();
    publisher.mergeObjectContext = jest.fn().mockImplementation((...args) => ({
      commit: () => {},
      verifyViaLDAP,
    }));
    await commandHandler.execute(
      new VerifyLDAPUserCommand('client', 'username', 'password'),
    );
    expect(manager.verifyViaLDAP).toHaveBeenCalledTimes(1);
  });
});
