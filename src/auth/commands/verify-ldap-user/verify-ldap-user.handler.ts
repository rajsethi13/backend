import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { VerifyLDAPUserCommand } from './verify-ldap-user.command';
import { LDAPClientAggregateService } from '../../aggregates/ldap-aggregate/ldap-client-aggregate.service';

@CommandHandler(VerifyLDAPUserCommand)
export class VerifyLDAPUserHandler
  implements ICommandHandler<VerifyLDAPUserCommand>
{
  constructor(
    private readonly manager: LDAPClientAggregateService,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: VerifyLDAPUserCommand) {
    const { ldapClient, username, password } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const { user } = await aggregate.verifyViaLDAP(
      ldapClient,
      username,
      password,
    );
    aggregate.commit();
    try {
      return {
        user: {
          ...user.toObject(),
          _id: undefined,
          otpPeriod: undefined,
          roles: undefined,
          deleted: undefined,
          creation: undefined,
        },
      };
    } catch (error) {
      return {
        user: { ...user, _id: undefined },
      };
    }
  }
}
