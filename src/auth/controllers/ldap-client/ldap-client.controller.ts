import {
  Controller,
  Post,
  UseGuards,
  ValidationPipe,
  UsePipes,
  Body,
  Req,
  Param,
  Get,
  Query,
  ForbiddenException,
  Res,
  HttpCode,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateLDAPClientDto } from './ldap-client-create.dto';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { UserService } from '../../../user-management/entities/user/user.service';
import { Roles } from '../../../common/decorators/roles.decorator';
import { RoleGuard } from '../../guards/role.guard';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import { LDAPClientGuard } from '../../guards/ldap-client.guard';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { AddLDAPClientCommand } from '../../commands/add-ldap-client/add-ldap-client.command';
import { LDAPClientService } from '../../entities/ldap-client/ldap-client.service';
import { ModifyLDAPClientCommand } from '../../commands/modify-ldap-client/modify-ldap-client.command';
import { RemoveLDAPClientCommand } from '../../commands/remove-ldap-client/remove-ldap-client.command';
import { VerifyLDAPUserCommand } from '../../commands/verify-ldap-user/verify-ldap-user.command';
import { QuerySort } from '../../../common/policies/list-query/sort.enum';
import { ErrorFilter } from '../../../common/filters/errors.filter';
import { LDAPClient } from '../../entities/ldap-client/ldap-client.interface';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';

@Controller('ldap_client')
export class LDAPClientController {
  constructor(
    private readonly ldapClient: LDAPClientService,
    private readonly userService: UserService,
    private readonly commandBus: CommandBus,
  ) {}

  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Create LDAP Client'),
    description: 'create account with ldap client',
  })
  async create(@Body() body: CreateLDAPClientDto, @Req() req) {
    const createdBy = req.user.user;
    return await this.commandBus.execute(
      new AddLDAPClientCommand(body, createdBy),
    );
  }

  @Post('v1/update/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update ldap client'),
    description: 'update ldap client details',
  })
  async update(
    @Body() payload: CreateLDAPClientDto,
    @Param('uuid') uuid: string,
  ) {
    return await this.commandBus.execute(
      new ModifyLDAPClientCommand(payload, uuid),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(@Query() query: ListQueryDto) {
    const { offset, limit, search, sort } = query;
    const where: { createdBy?: string } = {};
    const sortQuery = { name: sort || QuerySort.ASC };
    return await this.ldapClient.list(search, where, sortQuery, offset, limit);
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string, @Req() req) {
    let ldapClient: LDAPClient;
    if (await this.userService.checkAdministrator(req.user.user)) {
      ldapClient = await this.ldapClient.findOne({ uuid });
    } else {
      ldapClient = await this.ldapClient.findOne({
        uuid,
        createdBy: req.user.user,
      });
    }
    if (!ldapClient) throw new ForbiddenException();
    ldapClient.adminPassword = undefined;
    return ldapClient;
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteByUUID(@Param('uuid') uuid, @Req() req) {
    const userUuid = req.user.user;
    return await this.commandBus.execute<RemoveLDAPClientCommand>(
      new RemoveLDAPClientCommand(userUuid, uuid),
    );
  }

  @HttpCode(HttpStatus.OK)
  @Post('login/:ldapClient')
  @UseFilters(ErrorFilter)
  @UseGuards(LDAPClientGuard)
  login(@Body() body, @Req() req, @Res() res) {
    const out: { user: string; path?: string } = {
      user: req.user.email || req.user.phone,
    };
    if (body.redirect) {
      out.path = body.redirect;
      // https://stackoverflow.com/a/36885704
      // return res.redirect(out.path);
    }
    return res.json(out);
  }

  @HttpCode(HttpStatus.OK)
  @Post('verify_user/:ldapClient')
  @UseFilters(ErrorFilter)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async verifyUser(
    @Body('username') username: string,
    @Body('password') password: string,
    @Param('ldapClient') ldapClient: string,
  ) {
    return await this.commandBus.execute(
      new VerifyLDAPUserCommand(username, password, ldapClient),
    );
  }

  @Get('v1/list_clients')
  async listLDAPClients() {
    const clients = await this.ldapClient.find();
    return clients.map(login => ({
      name: login.name,
      uuid: login.uuid,
    }));
  }
}
