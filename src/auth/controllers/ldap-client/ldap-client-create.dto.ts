import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';

export class CreateLDAPClientDto {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Identifies a LDAP client uniquely'),
    required: true,
    example: 'Microsoft AD',
  })
  name: string;

  @IsOptional()
  @ApiProperty({
    description: i18n.__('LDAP Client description'),
    required: true,
    example: 'Microsoft AD Client',
  })
  description: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP Server URL'),
    required: true,
    type: 'string',
    example: 'ldap://ldap.example.com',
  })
  url?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Admin domain name'),
    required: true,
    example: 'IN/john.k',
  })
  adminDn?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP Admin DN password'),
    required: true,
    example: 'h4cv_4%b2#D:-)',
  })
  adminPassword?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP User search base'),
    required: true,
    type: 'string',
  })
  userSearchBase?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP User name attribute'),
    required: true,
    example: 'sn',
  })
  usernameAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP email attribute'),
    required: true,
    example: 'main',
  })
  emailAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP phone attribute'),
    required: true,
    example: 'mobile',
  })
  phoneAttribute?: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('LDAP full name attribute'),
    required: true,
    example: 'cn',
  })
  fullNameAttribute?: string;

  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('OAuth2 Client ID link'),
    type: 'string',
  })
  clientId: string;

  @ApiProperty({
    description: i18n.__('Additional user attributes to save as claims'),
  })
  @IsString({ each: true })
  attributes: string[];

  @IsString()
  @ApiProperty({
    description: i18n.__('Scope to store user claims'),
    required: true,
    type: 'string',
  })
  scope: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disabled LDAP Client'),
  })
  disabled?: boolean;
}
