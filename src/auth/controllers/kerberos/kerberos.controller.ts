import {
  Controller,
  Post,
  UseGuards,
  ValidationPipe,
  UsePipes,
  Body,
  Req,
  Param,
  Get,
  Query,
  ForbiddenException,
  Res,
  HttpCode,
  HttpStatus,
  UseFilters,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateKerberosRealmDto } from './kerberos-realm-create.dto';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { UserService } from '../../../user-management/entities/user/user.service';
import { Roles } from '../../../common/decorators/roles.decorator';
import { RoleGuard } from '../../guards/role.guard';
import { ListQueryDto } from '../../../common/policies/list-query/list-query';
import { KerberosGuard } from '../../guards/kerberos.guard';
import { BearerTokenGuard } from '../../guards/bearer-token.guard';
import { KerberosRealmService } from '../../entities/kerberos-realm/kerberos-realm.service';
import { QuerySort } from '../../../common/policies/list-query/sort.enum';
import { ErrorFilter } from '../../../common/filters/errors.filter';
import { KerberosRealm } from '../../entities/kerberos-realm/kerberos-realm.interface';
import { AddKerberosRealmCommand } from '../../commands/add-kerberos-realm/add-kerberos-realm.command';
import { ModifyKerberosRealmCommand } from '../../commands/modify-kerberos-realm/modify-kerberos-realm.command';
import { RemoveKerberosRealmCommand } from '../../commands/remove-kerberos-realm/remove-kerberos-realm.command';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';

@Controller('kerberos_realm')
export class KerberosController {
  constructor(
    private readonly kerberosRealm: KerberosRealmService,
    private readonly userService: UserService,
    private readonly commandBus: CommandBus,
  ) {}

  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Create Kerberos realm'),
    description: i18n.__('Create Kerberos realm'),
  })
  async create(@Body() body: CreateKerberosRealmDto, @Req() req) {
    const createdBy = req.user.user;
    return await this.commandBus.execute(
      new AddKerberosRealmCommand(body, createdBy),
    );
  }

  @Post('v1/update/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('Update kerberos realm'),
    description: 'update account with kerberos client',
  })
  async update(
    @Body() payload: CreateKerberosRealmDto,
    @Param('uuid') uuid: string,
  ) {
    return await this.commandBus.execute(
      new ModifyKerberosRealmCommand(payload, uuid),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async list(@Query() query: ListQueryDto) {
    const { offset, limit, search, sort } = query;
    const where: { createdBy?: string } = {};
    const sortQuery = { name: sort || QuerySort.ASC };
    return await this.kerberosRealm.list(
      search,
      where,
      sortQuery,
      offset,
      limit,
    );
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string, @Req() req) {
    let kerberosRealm: KerberosRealm;
    if (await this.userService.checkAdministrator(req.user.user)) {
      kerberosRealm = await this.kerberosRealm.findOne({ uuid });
    } else {
      kerberosRealm = await this.kerberosRealm.findOne({
        uuid,
        createdBy: req.user.user,
      });
    }
    if (!kerberosRealm) throw new ForbiddenException();
    return kerberosRealm;
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteByUUID(@Param('uuid') uuid, @Req() req) {
    const userUuid = req.user.user;
    return await this.commandBus.execute<RemoveKerberosRealmCommand>(
      new RemoveKerberosRealmCommand(userUuid, uuid),
    );
  }

  @HttpCode(HttpStatus.OK)
  @Post('login/:kerberosRealm')
  @UseFilters(ErrorFilter)
  @UseGuards(KerberosGuard)
  login(@Req() req, @Res() res, @Query() query) {
    return res.json({
      user: req.user.email || req.user.phone,
      path: query?.redirect,
    });
  }

  @Get('v1/list_realms')
  async listRealms() {
    const realms = await this.kerberosRealm.find();
    return realms.map(login => ({
      name: login.name,
      uuid: login.uuid,
    }));
  }
}
