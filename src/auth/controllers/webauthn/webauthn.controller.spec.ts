import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';
import { WebAuthnController } from './webauthn.controller';

describe('WebAuthn Controller', () => {
  let controller: WebAuthnController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WebAuthnController],
      providers: [
        { provide: CommandBus, useFactory: () => jest.fn() },
        { provide: QueryBus, useFactory: () => jest.fn() },
        { provide: BearerTokenService, useValue: {} },
      ],
    }).compile();

    controller = module.get<WebAuthnController>(WebAuthnController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
