import { Injectable } from '@nestjs/common';
import { JWK } from '../../middlewares/interfaces';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { OIDCKeyService } from '../../../auth/entities/oidc-key/oidc-key.service';
import { OIDCKey } from '../../../auth/entities/oidc-key/oidc-key.interface';
import {
  JWKS_ENDPOINT,
  OAUTH2_CONFIRMATION_ENDPOINT,
  OAUTH2_INTROSPECTION_ENDPOINT,
  OAUTH2_PROFILE_ENDPOINT,
  OAUTH2_REVOKE_ENDPOINT,
  OAUTH2_TOKEN_ENDPOINT,
} from '../../../constants/url-strings';

@Injectable()
export class WellKnownService {
  constructor(
    private readonly settingsService: ServerSettingsService,
    private readonly oidcKeyService: OIDCKeyService,
  ) {}

  async getOpenidConfiguration() {
    const settings = await this.settingsService.find();
    const authServerURL = settings.issuerUrl;
    return {
      issuer: authServerURL,
      jwks_uri: `${authServerURL}${JWKS_ENDPOINT}`,
      authorization_endpoint: `${authServerURL}${OAUTH2_CONFIRMATION_ENDPOINT}`,
      token_endpoint: `${authServerURL}${OAUTH2_TOKEN_ENDPOINT}`,
      userinfo_endpoint: `${authServerURL}${OAUTH2_PROFILE_ENDPOINT}`,
      revocation_endpoint: `${authServerURL}${OAUTH2_REVOKE_ENDPOINT}`,
      introspection_endpoint: `${authServerURL}${OAUTH2_INTROSPECTION_ENDPOINT}`,
      response_types_supported: [
        'id_token',
        'id_token token',
        'code id_token',
        'code token',
        'code id_token token',
      ],
      subject_types_supported: ['public'],
      id_token_signing_alg_values_supported: ['RS256', 'HS256'],
    };
  }

  async getJWKs() {
    const keys = await this.oidcKeyService.find();

    const out = keys.map((iKey: OIDCKey) => {
      const iKeyJWK: JWK = iKey.keyPair as JWK;
      const oKey: any = {};
      oKey.kty = iKeyJWK.kty;
      oKey.alg = iKeyJWK.alg;
      oKey.n = iKeyJWK.n;
      oKey.e = iKeyJWK.e;
      oKey.kid = iKeyJWK.kid;
      return oKey;
    });

    return {
      keys: out,
    };
  }
}
