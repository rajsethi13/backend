import internationalization from 'i18n';
import { join } from 'path';

internationalization.configure({
  directory:
    process.env.NODE_ENV === 'test'
      ? '/tmp'
      : join(process.cwd(), 'src', 'i18n'),
});

export const i18n = internationalization;
