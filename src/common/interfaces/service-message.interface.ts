import { ConnectedService } from './connected-service.interface';

export interface ServiceMessage {
  service: string;
  session: boolean;
  communication: boolean;
  services?: ConnectedService[];
  enableChoosingAccount?: boolean;
  enableUserPhone?: boolean;
  infrastructureConsoleClientId: string;
  issuerUrl?: string;
  logoUrl?: string;
  faviconUrl?: string;
  privacyUrl?: string;
  helpUrl?: string;
  termsUrl?: string;
  copyrightMessage?: string;
}
