import {
  ClientsProviderAsyncOptions,
  Transport,
  ClientProvider,
} from '@nestjs/microservices';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
  EVENTS_PASSWORD,
  EVENTS_CLIENT_ID,
  AllowedEventsProtocol,
} from '../config/config.service';

export const BROADCAST_EVENT = 'BROADCAST_EVENT';

export const useEventClientFactory = (
  config: ConfigService,
): ClientProvider => {
  if (
    config.get(EVENTS_PROTO) === AllowedEventsProtocol.MQTT &&
    config.get(EVENTS_HOST) &&
    config.get(EVENTS_PORT) &&
    config.get(EVENTS_USER) &&
    config.get(EVENTS_PASSWORD)
  ) {
    const url = `${config.get(EVENTS_PROTO)}://${config.get(
      EVENTS_USER,
    )}:${config.get(EVENTS_PASSWORD)}@${config.get(EVENTS_HOST)}:${config.get(
      EVENTS_PORT,
    )}`;
    const clientId = config.get(EVENTS_CLIENT_ID);
    return {
      transport: Transport.MQTT,
      options: { url, clientId, protocolVersion: 5 },
    };
  }
  if (
    config.get(EVENTS_PROTO) === AllowedEventsProtocol.TCP &&
    config.get(EVENTS_HOST) &&
    config.get(EVENTS_PORT)
  ) {
    return {
      transport: Transport.TCP,
      options: {
        port: Number(config.get(EVENTS_PORT)),
        host: config.get(EVENTS_HOST),
      },
    };
  }
  return {};
};

export const eventsClient: ClientsProviderAsyncOptions = {
  useFactory: useEventClientFactory,
  name: BROADCAST_EVENT,
  inject: [ConfigService],
};
