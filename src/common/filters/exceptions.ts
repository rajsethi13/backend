import { HttpStatus, HttpException } from '@nestjs/common';
import { i18n } from '../../i18n/i18n.config';

export class InvalidScopeException extends HttpException {
  constructor() {
    super(i18n.__('Invalid Scope'), HttpStatus.FORBIDDEN);
  }
}

export class InvalidClientException extends HttpException {
  constructor() {
    super(i18n.__('Invalid Client'), HttpStatus.FORBIDDEN);
  }
}

export class InvalidAuthorizationCodeException extends HttpException {
  constructor() {
    super(i18n.__('Invalid Authorization Code'), HttpStatus.FORBIDDEN);
  }
}

export class UserAlreadyExistsException extends HttpException {
  constructor() {
    super(i18n.__('User already exists'), HttpStatus.BAD_REQUEST);
  }
}

export class InvalidUserException extends HttpException {
  constructor() {
    super(i18n.__('Invalid User'), HttpStatus.BAD_REQUEST);
  }
}

export class TwoFactorEnabledException extends HttpException {
  constructor() {
    super(i18n.__('2FA already enabled'), HttpStatus.BAD_REQUEST);
  }
}

export class TwoFactorNotEnabledException extends HttpException {
  constructor() {
    super(i18n.__('2FA not enabled'), HttpStatus.BAD_REQUEST);
  }
}

export class InvalidOTPException extends HttpException {
  constructor() {
    super(i18n.__('Invalid OTP'), HttpStatus.BAD_REQUEST);
  }
}

export class SettingsNotFoundException extends HttpException {
  constructor() {
    super(i18n.__('Settings not found'), HttpStatus.BAD_REQUEST);
  }
}

export class JWKSNotFoundException extends HttpException {
  constructor() {
    super(i18n.__('JWKS not found'), HttpStatus.BAD_REQUEST);
  }
}

export class InvalidRoleException extends HttpException {
  constructor() {
    super(i18n.__('Invalid Role'), HttpStatus.BAD_REQUEST);
  }
}

export class CannotDeleteAdministratorException extends HttpException {
  constructor() {
    super(i18n.__('Cannot Delete Administrators'), HttpStatus.FORBIDDEN);
  }
}

export class InvalidCodeChallengeException extends HttpException {
  constructor() {
    super(i18n.__('Invalid Code Challenge'), HttpStatus.BAD_REQUEST);
  }
}

export class PasswordLessLoginNotEnabledException extends HttpException {
  constructor() {
    super(
      i18n.__('Password less login is not enabled'),
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class PasswordLessLoginAlreadyEnabledException extends HttpException {
  constructor() {
    super(
      i18n.__('Password less login is already enabled'),
      HttpStatus.BAD_REQUEST,
    );
  }
}

export class CommunicationServerNotFoundException extends HttpException {
  constructor() {
    super(
      i18n.__('Communication Server not found'),
      HttpStatus.NOT_IMPLEMENTED,
    );
  }
}

export class PhoneAlreadyRegisteredException extends HttpException {
  constructor() {
    super(i18n.__('Phone number already registered'), HttpStatus.BAD_REQUEST);
  }
}

export class EventsNotConnectedException extends HttpException {
  constructor() {
    super(i18n.__('Events service not connected'), HttpStatus.NOT_IMPLEMENTED);
  }
}

export class PhoneRegistrationNotAllowedException extends HttpException {
  constructor() {
    super(
      i18n.__('Server does not allow phone registration'),
      HttpStatus.NOT_IMPLEMENTED,
    );
  }
}

export class InvalidVerificationCode extends HttpException {
  constructor() {
    super(i18n.__('Invalid Verification Code'), HttpStatus.UNAUTHORIZED);
  }
}

export class EmailForVerificationNotFound extends HttpException {
  constructor() {
    super(i18n.__('Email for verification not found'), HttpStatus.NOT_FOUND);
  }
}

export class UserDeleteDisabled extends HttpException {
  constructor() {
    super(
      i18n.__('Deleting user account is disabled'),
      HttpStatus.UNAUTHORIZED,
    );
  }
}

export class InvalidRequestToDeleteUser extends HttpException {
  constructor() {
    super(i18n.__('user or client missing'), HttpStatus.UNAUTHORIZED);
  }
}

export class VerificationExpiredOrInvalid extends HttpException {
  constructor() {
    super(
      i18n.__(
        'Invalid or expired verification code used. Please signup again or reset password for the user.',
      ),
      HttpStatus.BAD_REQUEST,
    );
  }
}
