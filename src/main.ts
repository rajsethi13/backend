import { LogLevel } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { RAuthLogger } from './common/services/logger/r-auth-logger';
import { ConfigService, LOG_LEVEL } from './config/config.service';
import { API } from './constants/app-strings';
import {
  JWKS_ENDPOINT,
  OPENID_CONFIGURATION_ENDPOINT,
} from './constants/url-strings';
import { ExpressServer } from './express-server';

async function bootstrap() {
  const config = new ConfigService();
  const authServer = new ExpressServer(config);
  authServer.setupSecurity();
  authServer.setupI18n();

  const logLevels = (config.get(LOG_LEVEL)?.split(/\s*,\s*/) ||
    []) as LogLevel[];
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(authServer.server),
    { logger: new RAuthLogger({ logLevels }) },
  );

  // Set api prefix
  app.setGlobalPrefix(API, {
    exclude: [OPENID_CONFIGURATION_ENDPOINT, JWKS_ENDPOINT],
  });

  // Enable CORS
  app.enableCors();

  // Setup Swagger
  ExpressServer.setupSwagger(app);

  // Handlebars View engine
  ExpressServer.setupViewEngine(app);

  // Setup Cron
  ExpressServer.setupCron(config, app);

  // Setup Session
  authServer.setupSession(app);

  await app.listen(Number(process.env.PORT || '3000'));
}

bootstrap();
