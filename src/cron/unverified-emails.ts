import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { DeleteUnverifiedEmailsService } from '../auth/schedulers/delete-unverified-emails/delete-unverified-emails.service';

async function start() {
  const app = await NestFactory.create(AppModule);
  const cron = app.get(DeleteUnverifiedEmailsService);
  cron.start();
}

start();
