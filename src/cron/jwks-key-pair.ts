import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { KeyPairGeneratorService } from '../auth/schedulers';

async function start() {
  const app = await NestFactory.create(AppModule);
  const cron = app.get(KeyPairGeneratorService);
  cron.start();
}

start();
