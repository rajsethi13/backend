import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { AuthCodeSchedulerService } from '../auth/schedulers/auth-code-scheduler/auth-code-scheduler.service';

async function start() {
  const app = await NestFactory.create(AppModule);
  const cron = app.get(AuthCodeSchedulerService);
  cron.start();
}

start();
