import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { DeleteUnverifiedPhonesService } from '../auth/schedulers/delete-unverified-phones/delete-unverified-phones.service';

async function start() {
  const app = await NestFactory.create(AppModule);
  const cron = app.get(DeleteUnverifiedPhonesService);
  cron.start();
}

start();
