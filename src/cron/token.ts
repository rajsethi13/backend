import { NestFactory } from '@nestjs/core';
import { AppModule } from '../app.module';
import { TokenSchedulerService } from '../auth/schedulers';

async function start() {
  const app = await NestFactory.create(AppModule);
  const cron = app.get(TokenSchedulerService);
  cron.start();
}

start();
