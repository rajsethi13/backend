import { SystemSettingsChangedHandler } from './server-settings-changed/server-settings-changed.handler';
import { BearerTokensDeletedHandler } from './bearer-tokens-deleted/bearer-tokens-deleted.handler';
import { UserSessionsDeletedHandler } from './user-sessions-deleted/user-sessions-deleted.handler';
import { SystemLogAppendedHandler } from './system-log-appended/system-log-appended.handler';

export const SystemSettingsEventHandlers = [
  SystemSettingsChangedHandler,
  BearerTokensDeletedHandler,
  UserSessionsDeletedHandler,
  SystemLogAppendedHandler,
];
