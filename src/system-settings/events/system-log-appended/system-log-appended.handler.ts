import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SystemLogAppendedEvent } from './system-log-appended.event';
import { SystemLogService } from '../../entities/system-log/system-log.service';

@EventsHandler(SystemLogAppendedEvent)
export class SystemLogAppendedHandler
  implements IEventHandler<SystemLogAppendedEvent>
{
  constructor(private readonly log: SystemLogService) {}

  handle(event: SystemLogAppendedEvent) {
    this.log
      .save(event.log)
      .then(success => {})
      .catch(error => {});
  }
}
