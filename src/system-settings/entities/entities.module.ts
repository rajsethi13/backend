import { Module } from '@nestjs/common';
import { ServerSettingsService } from './server-settings/server-settings.service';
import { SystemSettingsModuleEntities } from './entities';
import { SystemLogService } from './system-log/system-log.service';

@Module({
  providers: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    SystemLogService,
  ],
  exports: [
    ...SystemSettingsModuleEntities,
    ServerSettingsService,
    SystemLogService,
  ],
})
export class SystemSettingsEntitiesModule {}
