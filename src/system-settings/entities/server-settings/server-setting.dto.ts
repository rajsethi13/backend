import {
  IsUrl,
  IsOptional,
  IsUUID,
  IsBoolean,
  IsNumber,
  Min,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import {
  THIRTY_NUMBER,
  TEN_NUMBER,
  ZERO_NUMBER,
} from '../../../constants/app-strings';

export class ServerSettingDto {
  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: 'The URL of the server.',
    type: 'string',
    required: true,
  })
  issuerUrl: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'OAuth 2.0 Client ID for Infrastructure Console',
    type: 'string',
  })
  infrastructureConsoleClientId?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket',
    type: 'string',
  })
  backupBucket?: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'disable signup or not',
    example: false,
  })
  disableSignup?: boolean;

  @IsOptional()
  @IsNumber()
  @ApiProperty({
    description: 'otp expiry number',
    type: 'number',
  })
  otpExpiry?: number;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: 'enable choosing account or not',
    example: false,
  })
  enableChoosingAccount?: boolean;

  @IsOptional()
  @IsNumber()
  @Min(THIRTY_NUMBER)
  @ApiProperty({
    description: 'refresh token expiry days',
    type: 'string',
  })
  refreshTokenExpiresInDays?: number;

  @IsOptional()
  @IsNumber()
  @Min(TEN_NUMBER)
  @ApiProperty({
    description: 'auth code expiry minutes',
    type: 'string',
  })
  authCodeExpiresInMinutes?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The name of host organization.',
    type: 'string',
  })
  organizationName: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow client to show custom login page',
    type: 'string',
  })
  enableCustomLogin: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Allow users to register phone',
    type: 'boolean',
  })
  enableUserPhone: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'Disallow user to be deleted',
    type: 'boolean',
  })
  isUserDeleteDisabled: boolean;

  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: 'system email account',
    type: 'string',
  })
  systemEmailAccount: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'url of logo',
    type: 'string',
  })
  logoUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'url of favicon',
    type: 'string',
  })
  faviconUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'privacy url',
    type: 'string',
  })
  privacyUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'help url',
    type: 'string',
  })
  helpUrl?: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: 'teams url',
    type: 'string',
  })
  termsUrl?: string;

  @IsOptional()
  @ApiProperty({
    description: 'copyright message',
    type: 'string',
  })
  copyrightMessage?: string;

  @IsOptional()
  @IsUUID()
  @ApiProperty({
    description: 'Cloud Storage Bucket for avatars',
    type: 'string',
  })
  avatarBucket?: string;

  @IsOptional()
  @IsNumber()
  @Min(ZERO_NUMBER)
  @ApiProperty({
    description: 'Disallow Login after number of failed attempts',
    type: 'number',
  })
  allowedFailedLoginAttempts?: number;
}
