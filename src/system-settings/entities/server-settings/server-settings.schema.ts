import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';
import { SERVICE } from '../../../constants/app-strings';

export const SERVER_SETTINGS_COLLECTION_NAME = 'server_settings';

export const ServerSettings = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    issuerUrl: String,
    organizationName: String,
    service: { type: String, default: SERVICE },
    infrastructureConsoleClientId: String,
    backupBucket: String,
    disableSignup: Boolean,
    otpExpiry: { type: Number, default: 5 },
    enableChoosingAccount: Boolean,
    enableCustomLogin: Boolean,
    refreshTokenExpiresInDays: { type: Number, default: 30 },
    authCodeExpiresInMinutes: { type: Number, default: 30 },
    enableUserPhone: { type: Boolean, default: false },
    isUserDeleteDisabled: { type: Boolean, default: false },
    systemEmailAccount: String,
    logoUrl: String,
    faviconUrl: String,
    privacyUrl: String,
    helpUrl: String,
    termsUrl: String,
    copyrightMessage: String,
    avatarBucket: String,
    allowedFailedLoginAttempts: { type: Number, default: 10 },
  },
  { collection: SERVER_SETTINGS_COLLECTION_NAME, versionKey: false },
);

export const SERVER_SETTINGS = 'ServerSettings';

export const ServerSettingsModel = mongoose.model(
  SERVER_SETTINGS,
  ServerSettings,
);
