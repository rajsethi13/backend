import { Document } from 'mongoose';

export class SystemLog extends Document {
  uuid?: string;
  data?:
    | {
        success?: unknown;
        error?: unknown;
        [key: string]: unknown;
      }
    | string;
  logType?: SystemLogType;
  entity?: string;
  entityId?: string;
  logCode?: SystemLogCode;
  reason?: string;
}

export enum SystemLogType {
  Authentication = 'Authentication',
  Email = 'Email',
}

export enum SystemLogCode {
  Success = 'Success',
  Failure = 'Failure',
}
