import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const SYSTEM_LOG_COLLECTION = 'system_log';

export const SystemLog = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    data: mongoose.Schema.Types.Mixed,
    logType: String,
    entity: String,
    entityId: String,
    logCode: String,
    reason: String,
  },
  { collection: SYSTEM_LOG_COLLECTION, versionKey: false, strict: false },
);

export const SYSTEM_LOG = 'SystemLog';

export const SystemLogModel = mongoose.model(SYSTEM_LOG, SystemLog);
