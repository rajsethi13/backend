import { Connection } from 'mongoose';
import {
  SERVER_SETTINGS,
  ServerSettings,
  SERVER_SETTINGS_COLLECTION_NAME,
} from './server-settings/server-settings.schema';
import { MONGOOSE_CONNECTION } from '../../common/database.provider';
import {
  SYSTEM_LOG,
  SYSTEM_LOG_COLLECTION,
  SystemLog,
} from './system-log/system-log.schema';

export const SystemSettingsModuleEntities = [
  {
    provide: SERVER_SETTINGS,
    useFactory: (connection: Connection) =>
      connection.model(
        SERVER_SETTINGS,
        ServerSettings,
        SERVER_SETTINGS_COLLECTION_NAME,
        { overwriteModels: process.env.NODE_ENV === 'test-e2e' },
      ),
    inject: [MONGOOSE_CONNECTION],
  },
  {
    provide: SYSTEM_LOG,
    useFactory: (connection: Connection) =>
      connection.model(SYSTEM_LOG, SystemLog, SYSTEM_LOG_COLLECTION),
    inject: [MONGOOSE_CONNECTION],
  },
];
