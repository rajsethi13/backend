import {
  IsEmail,
  IsUrl,
  IsNotEmpty,
  IsString,
  IsOptional,
} from 'class-validator';
import { IsMobileE164 } from '../../../common/decorators/is-mobile-e164.decorator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class SetupFormDTO {
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('Full Name'),
    required: true,
    example: 'John C',
  })
  fullName: string;

  @IsEmail()
  @ApiProperty({
    description: i18n.__('Set Administrator Email'),
    required: true,
    example: 'john@gmail.com',
  })
  email: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('Issuer URL'),
    required: true,
    type: 'string',
  })
  issuerUrl: string;

  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__(
      'Strong alphanumeric password, enriched with special characters',
    ),
    required: true,
    example: '#4hh3@123',
  })
  adminPassword: string;

  @IsMobileE164({ message: i18n.__('phone must be valid E164 phone number') })
  @ApiProperty({
    description: i18n.__('Phone number'),
    required: true,
    type: 'string',
  })
  phone: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Name of your organization'),
    required: true,
    type: 'string',
  })
  organizationName: string;
}
