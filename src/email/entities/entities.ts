import { MONGOOSE_CONNECTION } from '../../common/database.provider';
import { Connection } from 'mongoose';
import {
  EMAIL_ACCOUNT,
  EmailAccount,
} from './email-account/email-account.schema';

export const EmailModuleEntities = [
  {
    provide: EMAIL_ACCOUNT,
    useFactory: (connection: Connection) =>
      connection.model(EMAIL_ACCOUNT, EmailAccount),
    inject: [MONGOOSE_CONNECTION],
  },
];
