import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Req,
  Get,
  Query,
  Param,
} from '@nestjs/common';
import { Request } from 'express';

import { EmailService } from './email.service';
import { EmailMessageAuthServerDto } from './email-message-authserver-dto';
import { CreateEmailDto } from './create-email-dto';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { ApiOperation } from '@nestjs/swagger';
import { i18n } from '../../../i18n/i18n.config';

@Controller('email')
export class EmailController {
  constructor(private readonly emailService: EmailService) {}

  @Post('v1/system')
  @UseGuards(AuthServerVerificationGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('send system email'),
    description: i18n.__('send system email'),
  })
  async sendSystemEmail(@Body() payload: EmailMessageAuthServerDto) {
    return await this.emailService.sendSystemMessage(payload);
  }

  @Post('v1/trusted_client')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('send trusted client system email'),
    description: i18n.__('send Trusted Client System Message'),
  })
  async sendTrustedClientEmail(
    @Body() payload: EmailMessageAuthServerDto,
    @Req() request: Request & { token: unknown },
  ) {
    return await this.emailService.sendTrustedClientSystemMessage(
      payload,
      request.token,
    );
  }

  @Post('v1/create')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @ApiOperation({
    summary: i18n.__('Create Email account'),
    description: 'create email service',
  })
  async create(@Req() req, @Body() payload: CreateEmailDto) {
    return await this.emailService.createEmail(req, payload);
  }

  @Get('v1/list')
  @UseGuards(BearerTokenGuard)
  async list(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    const skip = Number(offset);
    const take = Number(limit);
    const query = {};
    return await this.emailService.list(skip, take, search, query, sort);
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @ApiOperation({
    summary: i18n.__('update email account'),
    description: i18n.__('update email account details'),
  })
  async update(@Body() payload: CreateEmailDto) {
    return await this.emailService.updateEmail(payload);
  }

  @Get('v1/find')
  @UseGuards(BearerTokenGuard)
  async findAll() {
    return await this.emailService.findAll();
  }

  @Get('v1/get/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async findOne(@Param('uuid') uuid: string) {
    const email = await this.emailService.findOne({ uuid });
    email.pass = undefined;
    return email;
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  async deleteEmailAccount(@Param('uuid') uuid: string) {
    return await this.emailService.deleteEmailAccount(uuid);
  }
}
