import { IEvent } from '@nestjs/cqrs';
import { EmailMessageAuthServerDto } from '../../../email/controllers/email/email-message-authserver-dto';
import { EmailAccount } from '../../entities/email-account/email-account.interface';

export class SystemEmailSentEvent implements IEvent {
  constructor(
    public readonly payload: EmailMessageAuthServerDto,
    public readonly email: EmailAccount,
  ) {}
}
