import {
  Body,
  Controller,
  Headers,
  Param,
  Post,
  Req,
  SerializeOptions,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { ApiOperation } from '@nestjs/swagger';

import { Scope } from '../../../auth/decorators/scope.decorator';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { ScopeGuard } from '../../../auth/guards/scope.guard';
import {
  AUTHORIZATION,
  SCOPE_SELF_SERVICE,
} from '../../../constants/app-strings';
import { i18n } from '../../../i18n/i18n.config';
import { AddClientCommand } from '../../commands/add-client/add-client.command';
import { ModifyClientCommand } from '../../commands/modify-client/modify-client.command';
import { RemoveOAuth2ClientCommand } from '../../commands/remove-oauth2client/remove-oauth2client.command';
import { UpdateClientSecretCommand } from '../../commands/update-client-secret/update-client-secret.command';
import { VerifyChangedClientSecretCommand } from '../../commands/verify-changed-client-secret/verify-changed-client-secret.command';
import { CreateClientDto } from '../../entities/client/create-client.dto';

@Controller('client/self_service')
@SerializeOptions({ excludePrefixes: ['_'] })
export class ClientSelfServiceController {
  constructor(private readonly commandBus: CommandBus) {}

  @Post('v1/create')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('create client'),
    description: 'create app client',
  })
  async create(@Body() body: CreateClientDto, @Req() req) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(new AddClientCommand(actorUuid, body));
  }

  @Post('v1/update/:clientId')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  @ApiOperation({
    summary: i18n.__('update client'),
    description: i18n.__('update with client details'),
  })
  async update(
    @Body() payload: CreateClientDto,
    @Param('clientId') clientId: string,
    @Req() req,
  ) {
    const actorUuid = req.user.user;
    return await this.commandBus.execute(
      new ModifyClientCommand(actorUuid, clientId, payload),
    );
  }

  @Post('v1/update_secret/:clientId')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async updateSecret(@Param('clientId') clientId: string, @Req() req) {
    const userUuid = req.user.user;
    return await this.commandBus.execute(
      new UpdateClientSecretCommand(userUuid, clientId),
    );
  }

  @Post('v1/verify_changed_secret')
  async verifyChangedSecret(@Headers(AUTHORIZATION) authorization) {
    return await this.commandBus.execute(
      new VerifyChangedClientSecretCommand(authorization),
    );
  }

  @Post('v1/delete/:clientId')
  @Scope(SCOPE_SELF_SERVICE)
  @UseGuards(BearerTokenGuard, ScopeGuard)
  async deleteByUUID(@Param('clientId') clientId, @Req() req) {
    const actorUserUuid = req.user.user;
    return await this.commandBus.execute(
      new RemoveOAuth2ClientCommand(actorUserUuid, clientId),
    );
  }
}
