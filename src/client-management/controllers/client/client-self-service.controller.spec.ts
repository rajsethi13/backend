import { Test, TestingModule } from '@nestjs/testing';
import { CommandBus } from '@nestjs/cqrs';
import { ClientSelfServiceController } from './client-self-service.controller';
import { ScopeGuard } from '../../../auth/guards/scope.guard';
import { BearerTokenService } from '../../../auth/entities/bearer-token/bearer-token.service';

describe('ClientSelfServiceController', () => {
  let module: TestingModule;
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [ClientSelfServiceController],
      providers: [
        {
          provide: CommandBus,
          useFactory: () => jest.fn(),
        },
        {
          provide: BearerTokenService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(ScopeGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: ClientSelfServiceController =
      module.get<ClientSelfServiceController>(ClientSelfServiceController);
    expect(controller).toBeDefined();
  });
});
