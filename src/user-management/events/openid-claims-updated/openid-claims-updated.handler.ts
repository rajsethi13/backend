import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OpenIDClaimsUpdatedEvent } from './openid-claims-updated.event';
import { UserClaimService } from '../../../auth/entities/user-claim/user-claim.service';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { SCOPE_OPENID } from '../../../constants/app-strings';

@EventsHandler(OpenIDClaimsUpdatedEvent)
export class OpenIDClaimsUpdatedHandler
  implements IEventHandler<OpenIDClaimsUpdatedEvent>
{
  constructor(private readonly userClaim: UserClaimService) {}
  handle(event: OpenIDClaimsUpdatedEvent) {
    const { userClaims, claims } = event;
    userClaims.forEach(async userClaim => {
      if (userClaim._id && !claims[userClaim.name]) {
        await this.deleteClaim(userClaim);
      } else if (userClaim._id && claims[userClaim.name]) {
        await this.updateClaim(userClaim, claims[userClaim.name]);
      } else if (!userClaim._id && userClaim.value) {
        await this.createClaim(userClaim);
      }
    });
  }

  async updateClaim(userClaim: UserClaim, claim: string) {
    return await this.userClaim.updateOne(
      { _id: userClaim._id },
      { value: claim },
    );
  }

  async deleteClaim(userClaim: UserClaim) {
    return await this.userClaim.deleteOne({ _id: userClaim._id });
  }

  async createClaim(userClaim: UserClaim) {
    const doc = await this.userClaim.findOne({
      uuid: userClaim.uuid,
      scope: SCOPE_OPENID,
      name: userClaim.name,
    });
    if (!doc) {
      return await this.userClaim.save(userClaim);
    }
    return await this.updateClaim(userClaim, userClaim.value as string);
  }
}
