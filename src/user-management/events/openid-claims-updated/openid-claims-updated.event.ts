import { IEvent } from '@nestjs/cqrs';
import { UserClaim } from '../../../auth/entities/user-claim/user-claim.interface';
import { OpenIDClaimsDTO } from '../../policies/openid-claims/openid-claims-dto';

export class OpenIDClaimsUpdatedEvent implements IEvent {
  constructor(
    public readonly userClaims: UserClaim[],
    public readonly claims: OpenIDClaimsDTO,
  ) {}
}
