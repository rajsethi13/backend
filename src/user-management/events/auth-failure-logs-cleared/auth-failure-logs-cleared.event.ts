import { IEvent } from '@nestjs/cqrs';

export class AuthFailureLogsClearedEvent implements IEvent {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
  ) {}
}
