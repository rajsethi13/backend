import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AuthFailureLogsClearedEvent } from './auth-failure-logs-cleared.event';
import { SystemLogService } from '../../../system-settings/entities/system-log/system-log.service';
import {
  SystemLogCode,
  SystemLogType,
} from '../../../system-settings/entities/system-log/system-log.interface';
import { USER } from '../../entities/user/user.schema';

@EventsHandler(AuthFailureLogsClearedEvent)
export class AuthFailureLogsClearedHandler
  implements IEventHandler<AuthFailureLogsClearedEvent>
{
  constructor(private readonly log: SystemLogService) {}
  handle(event: AuthFailureLogsClearedEvent) {
    const { userUuid } = event;
    this.log
      .deleteMany({
        entityId: userUuid,
        entity: USER,
        logType: SystemLogType.Authentication,
        logCode: SystemLogCode.Failure,
      })
      .then(success => {})
      .catch(err => {});
  }
}
