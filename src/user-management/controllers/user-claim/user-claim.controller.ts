import {
  Body,
  Controller,
  ForbiddenException,
  Get,
  Post,
  Query,
  Req,
  UseFilters,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { EventPattern, Payload } from '@nestjs/microservices';
import { Request } from 'express';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { Roles } from '../../../common/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { GetUserClaimsQuery } from '../../queries/get-user-claims/get-user-claims.query';
import { RpcExceptionFilter } from '../../../common/filters/rpc-exception.filter';
import { AddUserClaimCommand } from '../../commands/add-user-claim/add-user-claim.command';
import { RemoveUserClaimCommand } from '../../commands/remove-user-claim/remove-user-claim.command';
import { UpdateUserClaimCommand } from '../../commands/update-user-claim/update-user-claim.command';
import { ListUserClaimsDto } from './list-user-claims.dto';
import { UserClaimDto } from './user-claim.dto';
import { BearerTokenGuard } from '../../../auth/guards/bearer-token.guard';
import { BasicClientCredentialsGuard } from '../../../auth/guards/basic-client-credentials.guard';
import { Client } from '../../../client-management/entities/client/client.interface';
import { User } from '../../entities/user/user.interface';
import { i18n } from '../../../i18n/i18n.config';
import { ApiOperation } from '@nestjs/swagger';
export const UserClaimsAddedByServiceEvent = 'UserClaimsAddedByServiceEvent';
export const UserClaimsUpdatedByServiceEvent =
  'UserClaimsUpdatedByServiceEvent';
export const UserClaimsRemovedByServiceEvent =
  'UserClaimsRemovedByServiceEvent';

@Controller('user_claim')
export class UserClaimController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @UseFilters(new RpcExceptionFilter())
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @EventPattern(UserClaimsAddedByServiceEvent)
  async addUserClaim(@Payload() payload: UserClaimDto) {
    return await this.commandBus.execute(new AddUserClaimCommand(payload));
  }

  @UseFilters(new RpcExceptionFilter())
  @EventPattern(UserClaimsUpdatedByServiceEvent)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async updateUserClaim(@Payload() payload: UserClaimDto) {
    return await this.commandBus.execute(new UpdateUserClaimCommand(payload));
  }

  @UseFilters(new RpcExceptionFilter())
  @EventPattern(UserClaimsRemovedByServiceEvent)
  async removeUserClaim(@Payload() payload: { uuid: string; name: string }) {
    return await this.commandBus.execute(
      new RemoveUserClaimCommand(payload.uuid, payload.name),
    );
  }

  @Post('v1/client/add_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async addUserClaimHttpAsClient(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new AddUserClaimCommand(payload));
  }

  @Post('v1/client/update_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async updateUserClaimHttpAsClient(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new UpdateUserClaimCommand(payload));
  }

  @Post('v1/client/remove_user_claim')
  @UseGuards(BasicClientCredentialsGuard)
  async removeUserClaimHttpAsClient(
    @Body() payload: { uuid: string; name: string },
    @Req() request: Request & { client: Client },
  ) {
    if (!request.client?.isTrusted) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(
      new RemoveUserClaimCommand(payload.uuid, payload.name),
    );
  }

  @Get('v1/retrieve_user_claims')
  @Roles(ADMINISTRATOR)
  @UseGuards(BearerTokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async listUserClaims(@Query() query: ListUserClaimsDto) {
    return await this.queryBus.execute(
      new GetUserClaimsQuery(query.uuid, query.offset, query.limit),
    );
  }

  @Get('v1/get_user_claims')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  async getUserClaims(@Query() query: ListUserClaimsDto, @Req() req) {
    return await this.queryBus.execute(
      new GetUserClaimsQuery(req?.user?.user, query.offset, query.limit),
    );
  }

  @Post('v1/add_user_claim')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('add user claim'),
    description: i18n.__('add user claim for user'),
  })
  async addUserClaimHttp(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client; user: User },
  ) {
    if (!request.user || request.user?.uuid !== payload.uuid) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new AddUserClaimCommand(payload));
  }

  @Post('v1/update_user_claim')
  @UseGuards(BearerTokenGuard)
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @ApiOperation({
    summary: i18n.__('update user claim'),
    description: i18n.__('update user claim for user'),
  })
  async updateUserClaimHttp(
    @Body() payload: UserClaimDto,
    @Req() request: Request & { client: Client; user: User },
  ) {
    if (!request.user || request.user?.uuid !== payload.uuid) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(new UpdateUserClaimCommand(payload));
  }

  @Post('v1/remove_user_claim')
  @UseGuards(BearerTokenGuard)
  async removeUserClaimHttp(
    @Body() payload: { uuid: string; name: string },
    @Req() request: Request & { client: Client; user: User },
  ) {
    if (!request.user || request.user?.uuid !== payload.uuid) {
      throw new ForbiddenException();
    }
    return await this.commandBus.execute(
      new RemoveUserClaimCommand(payload.uuid, payload.name),
    );
  }
}
