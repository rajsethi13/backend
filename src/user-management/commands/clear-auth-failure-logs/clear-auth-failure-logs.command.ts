import { ICommand } from '@nestjs/cqrs';

export class ClearAuthFailureLogsCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly actorUuid: string,
  ) {}
}
