import { ICommand } from '@nestjs/cqrs';
import { OpenIDClaimsDTO } from '../../policies/openid-claims/openid-claims-dto';

export class UpdateOpenIDClaimsCommand implements ICommand {
  constructor(
    public readonly userUuid: string,
    public readonly claims: OpenIDClaimsDTO,
  ) {}
}
