import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { UserClaimAggregateService } from '../../aggregates/user-claim-aggregate/user-claim-aggregate.service';
import { UpdateOpenIDClaimsCommand } from './update-openid-claims.command';

@CommandHandler(UpdateOpenIDClaimsCommand)
export class UpdateOpenIDClaimsHandler
  implements ICommandHandler<UpdateOpenIDClaimsCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: UserClaimAggregateService,
  ) {}

  async execute(command: UpdateOpenIDClaimsCommand) {
    const { userUuid, claims } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateOpenIDClaims(userUuid, claims);
    aggregate.commit();
    return command;
  }
}
