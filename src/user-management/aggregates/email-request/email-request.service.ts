import { Injectable } from '@nestjs/common';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { handlebars } from 'hbs';
import { User } from '../../../user-management/entities/user/user.interface';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { AuthData } from '../../entities/auth-data/auth-data.interface';
import { i18n } from '../../../i18n/i18n.config';
import {
  FORGOT_ROUTE,
  SIGNUP_ROUTE,
  VERIFY_ROUTE,
} from '../../../constants/app-strings';
import { SendEmailService } from '../../../email/aggregates/send-email/send-email.service';

@Injectable()
export class EmailRequestService {
  constructor(
    private readonly sendEmail: SendEmailService,
    private readonly settings: ServerSettingsService,
  ) {}

  emailForgottenPasswordVerificationCode(
    user: User,
    verificationCode: AuthData,
  ): Observable<unknown> {
    return from(this.settings.find()).pipe(
      map(settings => {
        const generateForgottenPasswordUrl =
          settings.issuerUrl +
          FORGOT_ROUTE +
          '/' +
          verificationCode.password +
          (verificationCode.metaData?.redirect
            ? '?redirect=' +
              encodeURIComponent(verificationCode.metaData?.redirect)
            : '');
        const txtMessage =
          'Visit the following link to generate new password\n' +
          generateForgottenPasswordUrl;
        const htmlMessage = `To generate new password <a href='${generateForgottenPasswordUrl}'>click here</a>`;

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: 'Forgot Password? Generate new password for ' + user.email,
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  emailRequest(unverifiedUser: User, verificationCode: AuthData) {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const verificationUrl =
          settings.issuerUrl +
          SIGNUP_ROUTE +
          '/' +
          verificationCode.password +
          (verificationCode.metaData?.redirect
            ? '?redirect=' +
              encodeURIComponent(verificationCode.metaData?.redirect)
            : '');

        const txtMessage =
          'Visit the following link to complete signup\n' + verificationUrl;
        const htmlMessage = this.getSignupHTML(
          `To complete signup <a href='{{ verificationUrl }}'>click here</a>`,
          verificationUrl,
        );

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: unverifiedUser.email,
          subject:
            i18n.__('Please complete sign up for ') + unverifiedUser.email,
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  getSignupHTML(template: string, verificationUrl: string) {
    const renderer = handlebars.compile(template);
    return renderer({ verificationUrl });
  }

  verifyEmail(email: AuthData, code: AuthData) {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const verificationUrl =
          settings.issuerUrl + VERIFY_ROUTE + '/' + code.password;

        const txtMessage =
          'Visit the following link to complete email verification\n' +
          verificationUrl;
        const htmlMessage = this.getSignupHTML(
          `To complete verification <a href='{{ verificationUrl }}'>click here</a>`,
          verificationUrl,
        );

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: email.password,
          subject:
            i18n.__('Please complete verification for ') + email.password,
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  sendUnverifiedEmailVerificationCode(
    user: User,
    verificationCode: AuthData,
  ): Observable<unknown> {
    // Send Email
    return from(this.settings.find()).pipe(
      map(settings => {
        const generateForgottenPasswordUrl =
          settings.issuerUrl + VERIFY_ROUTE + '/' + verificationCode.password;
        const txtMessage =
          'Visit the following link to verify email\n' +
          generateForgottenPasswordUrl;
        const htmlMessage = `To verify email <a href='${generateForgottenPasswordUrl}'>click here</a>`;

        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: 'Complete email verification for ' + user.email,
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }

  sendOTP(user: User, hotp: string, otp: AuthData) {
    return from(this.settings.find()).pipe(
      map(settings => {
        let txtMessage =
          'OTP for login for ' + user.email + ' is ' + hotp + '\n';
        txtMessage += 'OTP expires at ' + otp.expiry + '. Do not share otp.';
        const htmlMessage = txtMessage.replace('\n', '<br>');
        return this.sendEmail.sendEmailWithSettings(settings, {
          emailTo: user.email,
          subject: 'Complete email verification for ' + user.email,
          text: txtMessage,
          html: htmlMessage,
        });
      }),
    );
  }
}
