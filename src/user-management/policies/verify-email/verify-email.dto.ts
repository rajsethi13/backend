import { IsString, IsNotEmpty } from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class VerifyEmailDto {
  @IsString()
  @ApiProperty({
    description: i18n.__('Verification Code'),
    required: true,
    type: 'string',
  })
  verificationCode: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('User password'),
    required: true,
    type: 'string',
  })
  password: string;
}
