import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { S3 } from '@aws-sdk/client-s3';
import { BucketFileDeletedEvent } from './bucket-file-deleted.event';

@EventsHandler(BucketFileDeletedEvent)
export class BucketFileDeletedHandler
  implements IEventHandler<BucketFileDeletedEvent>
{
  handle(event: BucketFileDeletedEvent) {
    const s3 = new S3({
      region: event.storage.region,
      endpoint: event.storage.endpoint,
      credentials: {
        accessKeyId: event.storage.accessKey,
        secretAccessKey: event.storage.secretKey,
      },
    });

    // remove a file from bucket
    const params = {
      Bucket: event.storage.bucket,
      Key: event.storage.basePath + '/' + event.filename,
    };
    s3.deleteObject(params, (err, data) => {});
  }
}
