import { MONGOOSE_CONNECTION } from '../../common/database.provider';
import { Connection } from 'mongoose';
import { STORAGE, Storage } from './storage/storage.schema';

export const CloudStorageModuleEntities = [
  {
    provide: STORAGE,
    useFactory: (connection: Connection) => connection.model(STORAGE, Storage),
    inject: [MONGOOSE_CONNECTION],
  },
];
