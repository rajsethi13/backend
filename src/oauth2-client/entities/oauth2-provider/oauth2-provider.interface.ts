import { Document } from 'mongoose';

export interface OAuth2Provider extends Document {
  uuid?: string;
  name?: string;
  authServerURL?: string;
  clientId?: string;
  clientSecret?: string;
  profileURL?: string;
  tokenURL?: string;
  introspectionURL?: string;
  authorizationURL?: string;
  revocationURL?: string;
  scope?: string[];
  disabled?: boolean;
}
