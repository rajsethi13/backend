import { Test, TestingModule } from '@nestjs/testing';
import { OAuth2ProviderService } from './oauth2-provider.service';
import { OAUTH2_PROVIDER } from './oauth2-provider.schema';

describe('OAuth2ProviderService', () => {
  let service: OAuth2ProviderService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OAuth2ProviderService,
        {
          provide: OAUTH2_PROVIDER,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<OAuth2ProviderService>(OAuth2ProviderService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
