import { Document } from 'mongoose';

export interface OAuth2Token extends Document {
  uuid?: string;
  oauth2Provider?: string; // uuid of Social Key
  user?: string; // uuid of request user;
  accessToken?: string;
  refreshToken?: string;
  idToken?: string;
}
