import mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

export const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, unique: true, sparse: true },
    oauth2Provider: String, // uuid of Social Key
    user: String, // uuid of request user;
    accessToken: String,
    refreshToken: String,
    idToken: String,
  },
  { collection: 'oauth2_token', versionKey: false },
);

export const Oauth2Token = schema;

export const OAUTH2_TOKEN = 'Oauth2Token';

export const Oauth2TokenModel = mongoose.model(OAUTH2_TOKEN, Oauth2Token);
