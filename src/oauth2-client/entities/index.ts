import { Connection } from 'mongoose';
import {
  OAUTH2_PROVIDER,
  OAuth2Provider,
} from './oauth2-provider/oauth2-provider.schema';
import { OAuth2ProviderService } from './oauth2-provider/oauth2-provider.service';
import { OAuth2TokenService } from './oauth2-token/oauth2-token.service';
import { MONGOOSE_CONNECTION } from '../../common/database.provider';
import { OAUTH2_TOKEN, Oauth2Token } from './oauth2-token/oauth2-token.schema';

export const OAuth2ClientEntityProviders = [
  OAuth2ProviderService,
  OAuth2TokenService,
];

export const OAuth2ClientEntities = [
  {
    provide: OAUTH2_PROVIDER,
    useFactory: (connection: Connection) =>
      connection.model(OAUTH2_PROVIDER, OAuth2Provider),
    inject: [MONGOOSE_CONNECTION],
  },
  {
    provide: OAUTH2_TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(OAUTH2_TOKEN, Oauth2Token),
    inject: [MONGOOSE_CONNECTION],
  },
];
