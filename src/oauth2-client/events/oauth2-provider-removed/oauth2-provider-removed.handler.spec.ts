import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { OAuth2Provider } from '../../entities/oauth2-provider/oauth2-provider.interface';
import { OAuth2ProviderRemovedHandler } from './oauth2-provider-removed.handler';
import { OAuth2ProviderRemovedEvent } from './oauth2-provider-removed.event';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';

describe('Event: OAuth2ProviderRemovedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: OAuth2ProviderRemovedHandler;
  let provider: OAuth2ProviderService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        OAuth2ProviderRemovedHandler,
        {
          provide: OAuth2ProviderService,
          useFactory: () => jest.fn(),
        },
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<OAuth2ProviderRemovedHandler>(
      OAuth2ProviderRemovedHandler,
    );
    provider = module.get<OAuth2ProviderService>(OAuth2ProviderService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should remove OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    provider.delete = jest.fn(() => Promise.resolve({}));
    await eventHandler.handle(
      new OAuth2ProviderRemovedEvent({} as OAuth2Provider),
    );
    expect(provider.delete).toHaveBeenCalledTimes(1);
  });
});
