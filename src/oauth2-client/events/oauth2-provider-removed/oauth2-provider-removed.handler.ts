import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { OAuth2ProviderRemovedEvent } from './oauth2-provider-removed.event';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';

@EventsHandler(OAuth2ProviderRemovedEvent)
export class OAuth2ProviderRemovedHandler
  implements IEventHandler<OAuth2ProviderRemovedEvent>
{
  constructor(private readonly provider: OAuth2ProviderService) {}
  handle(event: OAuth2ProviderRemovedEvent) {
    const { provider } = event;
    this.provider.delete(provider).then().catch();
  }
}
