import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { UpdateWriteOpResult } from 'mongoose';
import { OAuth2ProviderUpdatedHandler } from './oauth2-provider-updated.handler';
import { OAuth2ProviderUpdatedEvent } from './oauth2-provider-updated.event';
import { OAuth2Provider } from '../../entities/oauth2-provider/oauth2-provider.interface';
import { OAuth2ProviderService } from '../../entities/oauth2-provider/oauth2-provider.service';

describe('Event: OAuth2ProviderUpdatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: OAuth2ProviderUpdatedHandler;
  let provider: OAuth2ProviderService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        OAuth2ProviderUpdatedHandler,
        {
          provide: OAuth2ProviderService,
          useFactory: () => jest.fn(),
        },
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<OAuth2ProviderUpdatedHandler>(
      OAuth2ProviderUpdatedHandler,
    );
    provider = module.get<OAuth2ProviderService>(OAuth2ProviderService);
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    provider.update = jest.fn(() => Promise.resolve({} as UpdateWriteOpResult));
    await eventHandler.handle(
      new OAuth2ProviderUpdatedEvent({} as OAuth2Provider),
    );
    expect(provider.update).toHaveBeenCalledTimes(1);
  });
});
