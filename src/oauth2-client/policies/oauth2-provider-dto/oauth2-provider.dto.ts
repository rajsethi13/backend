import {
  IsString,
  IsUrl,
  IsNotEmpty,
  IsOptional,
  IsArray,
  IsUUID,
  IsBoolean,
} from 'class-validator';
import { i18n } from '../../../i18n/i18n.config';
import { ApiProperty } from '@nestjs/swagger';

export class OAuth2ProviderDto {
  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: i18n.__('Random generated uuid'),
    required: true,
    type: 'string',
  })
  uuid: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Name'),
    required: true,
    example: 'google',
  })
  name: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Authorization Server URL'),
    required: true,
    type: 'string',
  })
  authServerURL: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Client ID'),
    required: true,
    type: 'string',
  })
  clientId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Client Secret'),
    required: true,
    type: 'string',
  })
  clientSecret: string;

  @IsUrl({ require_tld: false })
  @IsOptional()
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Profile URL'),
    required: true,
    type: 'string',
  })
  profileURL: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Token URL'),
    required: true,
    type: 'string',
  })
  tokenURL: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Authorization URL'),
    required: true,
    type: 'string',
  })
  authorizationURL: string;

  @IsUrl({ require_tld: false })
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider Token Revocation URL'),
    required: true,
    type: 'string',
  })
  revocationURL: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  @ApiProperty({
    description: i18n.__('OAuth 2 Provider scopes'),
    required: true,
    type: 'array',
    example: ['openid', 'email', 'profile'],
  })
  scope: string[];

  @IsOptional()
  @IsBoolean()
  @ApiProperty({
    description: i18n.__('Disable OAuth 2 Provider'),
  })
  disabled?: boolean;
}
