import { Module, Global } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { Oauth2ProviderController } from './controllers/oauth2-provider/oauth2-provider.controller';
import { OAuth2ClientEntities, OAuth2ClientEntityProviders } from './entities';
import { OAuth2ClientAggregates } from './aggregates';
import { OAuth2ClientCommandHandlers } from './commands';
import { OAuth2ClientQueryHandlers } from './queries';
import { OAuth2ClientEventHandlers } from './events';
import { CqrsModule } from '@nestjs/cqrs';

Global();
@Module({
  imports: [CqrsModule, HttpModule],
  providers: [
    ...OAuth2ClientEntities,
    ...OAuth2ClientEntityProviders,
    ...OAuth2ClientAggregates,
    ...OAuth2ClientCommandHandlers,
    ...OAuth2ClientQueryHandlers,
    ...OAuth2ClientEventHandlers,
  ],
  controllers: [Oauth2ProviderController],
})
export class Oauth2ClientModule {}
